const status = require('http-status');
const { validationResult } = require("express-validator");

// Internal Dependencies
const LanguageHelper = require('../../../helpers/languageHelper');

const LanguageRoutes = {};

LanguageRoutes.get = async (req, res, next) => {
  try {
    const params = req.body;
    const data = await LanguageHelper.findAll(params, true);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

LanguageRoutes.getOne = async (req, res, next) => {
  try {
    const id = req.params.id;
    const data = await LanguageHelper.findOne(id);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

LanguageRoutes.update = async (req, res, next) => {
  try {
    const id = req.params.id;
    const update = req.body;
    const data = await LanguageHelper.findAndUpdate(id, update);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

LanguageRoutes.create = async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if(errors && errors.errors.length) {
      return res.status(status.BAD_REQUEST).send(errors.errors);
    }
    const document = req.body;
    document.createdBy = req.user._id;
    const data = await LanguageHelper.create(document);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

LanguageRoutes.delete = async (req, res, next) => {
  try {
    const id = req.params.id;
    const user_id = req.user._id
    const data = await LanguageHelper.delete(id, user_id);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

module.exports = LanguageRoutes;
