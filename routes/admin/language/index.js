const express = require('express');
const router = express.Router();
const LanguageRoutes = require('./language');
const { checkSchema } = require('express-validator');
const { insertLanguageSchema } = require('./language.shema');

router.get('/:id', LanguageRoutes.getOne)
router.post('/', LanguageRoutes.get);
router.post('/create', checkSchema(insertLanguageSchema), LanguageRoutes.create);
router.put('/:id', checkSchema(insertLanguageSchema), LanguageRoutes.update);
router.delete('/:id', LanguageRoutes.delete);

module.exports = router;
