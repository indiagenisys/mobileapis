exports.insertLanguageSchema = {
  name: {
    in: ['body'],
    notEmpty: true,
    errorMessage: 'name is required',
  },
  code: {
    in: ['body'],
    notEmpty: true,
    errorMessage: 'code is required',
  },
  country: {
    in: ['body'],
    notEmpty: true,
    errorMessage: 'country is required',
  },
};
