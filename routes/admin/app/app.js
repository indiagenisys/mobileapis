const status = require('http-status');
const { validationResult } = require("express-validator");

// Internal Dependencies
const AppHelper = require('../../../helpers/appHelper');

const AppRoutes = {};

AppRoutes.get = async (req, res, next) => {
  try {
    const params = req.body;
    const data = await AppHelper.findAll(params, true);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

AppRoutes.getOne = async (req, res, next) => {
  try {
    const params = req.params;
    const data = await AppHelper.findAll(params, true, true);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

AppRoutes.update = async (req, res, next) => {
  try {
    const id = req.params.id;
    const update = req.body;
    const data = await AppHelper.findAndUpdate(id, update);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

AppRoutes.create = async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if(errors && errors.errors.length) {
      return res.status(status.BAD_REQUEST).send(errors.errors);
    }
    const document = req.body;
    document.createdBy = req.user._id;
    const data = await AppHelper.create(document);
    res.status(data.code).send(data);
  } catch (error) {
    console.log('Err : ', error);
    next(error);
  }
};

AppRoutes.delete = async (req, res, next) => {
  try {
    const id = req.params.id;
    const user_id = req.user._id
    const data = await AppHelper.delete(id, user_id);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

module.exports = AppRoutes;
