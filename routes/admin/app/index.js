const express = require('express');
const router = express.Router();
const appRoutes = require('./app');
const { checkSchema } = require('express-validator');
const { insertAppSchema } = require('./app.schema');

router.get('/:id', appRoutes.getOne)
router.post('/', appRoutes.get);
router.post('/create', checkSchema(insertAppSchema), appRoutes.create);
router.put('/:id', checkSchema(insertAppSchema), appRoutes.update);
router.delete('/:id', appRoutes.delete);

module.exports = router;
