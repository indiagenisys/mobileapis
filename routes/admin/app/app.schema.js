exports.insertAppSchema = {
    name: {
      in: ['body'],
      notEmpty: true,
      errorMessage: 'name is required',
    },
    packageName: {
      in: ['body'],
      notEmpty: true,
      errorMessage: 'packageName is required',
    }
  };
  