const express = require('express');
const router = express.Router();
const { checkSchema } = require('express-validator');
const validationSchema = require('../../auth/register.schema');
const upload = require('../../../middleware/multiupload');

/* Internal routers */
const user = require('./user');

/* GET users listing. */
router.post('/', user.get);
router.get('/:id', user.getOne);
router.post('/create', upload.single('profileImage'), checkSchema(validationSchema.RegisterSchema), user.create);
router.put('/:id', upload.single('profileImage'), user.update);

module.exports = router;