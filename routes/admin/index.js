var express = require('express');

const { Roles } = require('../../constants/enum');
const auth = require('../../middleware/auth');

var router = express.Router();

const userRoutes = require('./users');
const languageRoutes = require('./language');
const categoryRoutes = require('./category');
const tagRoutes = require('./tag');
const itemRoutes = require('./item');
const subItemRoutes = require('./subItem');
const appRoutes = require('./app');
const storeRoutes = require('./store');
const homeActivityRoutes = require('./homeActivity');

router.use('/users', auth([Roles.Admin]), userRoutes);
router.use('/language', auth([Roles.Admin, Roles.ContentManager]), languageRoutes);
router.use('/category', auth([Roles.Admin, Roles.ContentManager]), categoryRoutes);
router.use('/tag', auth([Roles.Admin, Roles.ContentManager]), tagRoutes);
router.use('/item', auth([Roles.Admin, Roles.ContentManager]), itemRoutes);
router.use('/sub-item', auth([Roles.Admin, Roles.ContentManager]), subItemRoutes);
router.use('/app', auth([Roles.Admin, Roles.ContentManager]), appRoutes);
router.use('/store', auth([Roles.Admin, Roles.ContentManager]), storeRoutes);
router.use('/home-activity', auth([Roles.Admin, Roles.ContentManager]), homeActivityRoutes);

module.exports = router;