exports.insertHomeActivitySchema = {
  title: {
    in: ['body'],
    notEmpty: true,
    errorMessage: 'title is required',
  },
  type: {
    in: ['body'],
    notEmpty: true,
    errorMessage: 'packageName is required',
  },
  sequence: {
    in: ['body'],
    notEmpty: true,
    errorMessage: 'sequence is required',
  }
};
  