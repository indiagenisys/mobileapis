const status = require('http-status');
const { validationResult } = require("express-validator");

// Internal Dependencies
const HomeActivityHelper = require('../../../helpers/homeActivityHelper');

const HomeActivityRoutes = {};

HomeActivityRoutes.get = async (req, res, next) => {
  try {
    const params = req.body;
    const data = await HomeActivityHelper.findAll(params, true);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

HomeActivityRoutes.getOne = async (req, res, next) => {
  try {
    const params = req.params;
    const data = await HomeActivityHelper.findAll(params, true, true);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

HomeActivityRoutes.update = async (req, res, next) => {
  try {
    const id = req.params.id;
    const update = req.body;
    const data = await HomeActivityHelper.findAndUpdate(id, update);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

HomeActivityRoutes.create = async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if(errors && errors.errors.length) {
      return res.status(status.BAD_REQUEST).send(errors.errors);
    }
    const document = req.body;
    document.createdBy = req.user._id;
    const data = await HomeActivityHelper.create(document);
    res.status(data.code).send(data);
  } catch (error) {
    console.log('Err : ', error);
    next(error);
  }
};

HomeActivityRoutes.delete = async (req, res, next) => {
  try {
    const id = req.params.id;
    const user_id = req.user._id
    const data = await HomeActivityHelper.delete(id, user_id);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

module.exports = HomeActivityRoutes;
