const express = require('express');
const router = express.Router();
const homeActivityRoutes = require('./home-activity');
const { checkSchema } = require('express-validator');
const { insertHomeActivitySchema } = require('./home-activity.schema');

router.get('/:id', homeActivityRoutes.getOne)
router.post('/', homeActivityRoutes.get);
router.post('/create', checkSchema(insertHomeActivitySchema), homeActivityRoutes.create);
router.put('/:id', checkSchema(insertHomeActivitySchema), homeActivityRoutes.update);
router.delete('/:id', homeActivityRoutes.delete);

module.exports = router;
