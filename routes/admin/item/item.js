const status = require('http-status');
const { validationResult } = require("express-validator");

// Internal Dependencies
const itemHelper = require('../../../helpers/itemHelper');

const itemRoutes = {};

itemRoutes.get = async (req, res, next) => {
  try {
    const params = req.body;
    const data = await itemHelper.findAll(params, true);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

itemRoutes.getOne = async (req, res, next) => {
  try {
    const id = req.params.id;
    const data = await itemHelper.findAll({ id }, true, true);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

itemRoutes.update = async (req, res, next) => {
  try {
    const id = req.params.id;
    const update = req.body;
    const data = await itemHelper.findAndUpdate(id, update);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

itemRoutes.create = async (req, res, next) => {
  try {
    const document = req.body;
    document.createdBy = req.user._id;
    document.ownedBy = req.user._id;
    const data = await itemHelper.create(document);
    res.status(data.code).send(data);
  } catch (error) {
    console.log('\n error : ', error);
    next(error);
  }
};

itemRoutes.delete = async (req, res, next) => {
  try {
    const id = req.params.id;
    const user_id = req.user._id
    const data = await itemHelper.delete(id, user_id);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

module.exports = itemRoutes;
