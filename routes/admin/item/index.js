const express = require('express');
const router = express.Router();
const itemRoutes = require('./item');

router.get('/:id', itemRoutes.getOne)
router.post('/', itemRoutes.get);
router.post('/create', itemRoutes.create);
router.put('/:id', itemRoutes.update);
router.delete('/:id', itemRoutes.delete);

module.exports = router;
