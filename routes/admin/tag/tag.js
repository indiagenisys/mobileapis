const status = require('http-status');
const { validationResult } = require("express-validator");

// Internal Dependencies
const TagHelper = require('../../../helpers/tagHelper');

const TagRoutes = {};

TagRoutes.get = async (req, res, next) => {
  try {
    const params = req.body;
    const data = await TagHelper.findAll(params, true);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

TagRoutes.getOne = async (req, res, next) => {
  try {
    const params = req.params;
    const data = await TagHelper.findAll(params, true, true);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

TagRoutes.update = async (req, res, next) => {
  try {
    const id = req.params.id;
    const update = req.body;
    const data = await TagHelper.findAndUpdate(id, update);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

TagRoutes.create = async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if(errors && errors.errors.length) {
      return res.status(status.BAD_REQUEST).send(errors.errors);
    }
    const document = req.body;
    document.createdBy = req.user._id;
    const data = await TagHelper.create(document);
    res.status(data.code).send(data);
  } catch (error) {
    console.log('Err : ', error);
    next(error);
  }
};

TagRoutes.delete = async (req, res, next) => {
  try {
    const id = req.params.id;
    const user_id = req.user._id
    const data = await TagHelper.delete(id, user_id);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

module.exports = TagRoutes;
