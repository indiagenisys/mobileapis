const express = require('express');
const router = express.Router();
const TagRoutes = require('./tag');
const { checkSchema } = require('express-validator');
const { insertTagSchema } = require('./tag.schema');

router.get('/:id', TagRoutes.getOne)
router.post('/', TagRoutes.get);
router.post('/create', TagRoutes.create);
router.put('/:id', checkSchema(insertTagSchema), TagRoutes.update);
router.delete('/:id', TagRoutes.delete);

module.exports = router;
