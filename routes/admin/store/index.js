const express = require('express');
const router = express.Router();
const storeRoutes = require('./store');
const { checkSchema } = require('express-validator');
const { insertStoreSchema } = require('./store.schema');

router.get('/:id', storeRoutes.getOne)
router.post('/', storeRoutes.get);
router.post('/create', checkSchema(insertStoreSchema), storeRoutes.create);
router.put('/:id', checkSchema(insertStoreSchema), storeRoutes.update);
router.delete('/:id', storeRoutes.delete);

module.exports = router;
