exports.insertStoreSchema = {
    name: {
      in: ['body'],
      notEmpty: true,
      errorMessage: 'name is required',
    },
    baseUrl: {
      in: ['body'],
      notEmpty: true,
      errorMessage: 'base url is required',
    }
  };
  