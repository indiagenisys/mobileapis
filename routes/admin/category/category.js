const status = require('http-status');
const { validationResult } = require("express-validator");

// Internal Dependencies
const CategoryHelper = require('../../../helpers/categoryHelper');

const CategoryRoutes = {};

CategoryRoutes.get = async (req, res, next) => {
  try {
    const params = req.body;
    const data = await CategoryHelper.findAll(params, true);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

CategoryRoutes.getCategoryByParent = async (req, res, next) => {
  try {
    const { params } = req;
    const data = await CategoryHelper.findCategoryByParent(params, true);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

CategoryRoutes.getOne = async (req, res, next) => {
  try {
    const params = req.params;
    const data = await CategoryHelper.findAll(params, true, true);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

CategoryRoutes.update = async (req, res, next) => {
  try {
    const id = req.params.id;
    const update = req.body;
    const data = await CategoryHelper.findAndUpdate(id, update);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

CategoryRoutes.create = async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if(errors && errors.errors.length) {
      return res.status(status.BAD_REQUEST).send(errors.errors);
    }
    const document = req.body;
    document.createdBy = req.user._id;
    const data = await CategoryHelper.create(document);
    res.status(data.code).send(data);
  } catch (error) {
    console.log('Err : ', error);
    next(error);
  }
};

CategoryRoutes.delete = async (req, res, next) => {
  try {
    const id = req.params.id;
    const user_id = req.user._id
    const data = await CategoryHelper.delete(id, user_id);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

module.exports = CategoryRoutes;
