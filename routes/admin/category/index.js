const express = require('express');
const router = express.Router();
const CategoryRoutes = require('./category');
const { checkSchema } = require('express-validator');
const { insertCategorySchema } = require('./category.schema');
const upload = require('../../../middleware/multiupload');

router.get('/parent/:id', CategoryRoutes.getCategoryByParent)
router.get('/:id', CategoryRoutes.getOne)
router.post('/', CategoryRoutes.get);
router.post('/create', upload.single('image'), CategoryRoutes.create);
router.put('/:id', upload.single('image'), checkSchema(insertCategorySchema), CategoryRoutes.update);
router.delete('/:id', CategoryRoutes.delete);

module.exports = router;
