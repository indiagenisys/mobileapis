const express = require('express');
const router = express.Router();
const subItemRoutes = require('./subItem');

router.get('/:id', subItemRoutes.getOne)
router.post('/', subItemRoutes.get);
router.post('/create', subItemRoutes.create);
router.put('/:id', subItemRoutes.update);
router.delete('/:id', subItemRoutes.delete);

module.exports = router;
