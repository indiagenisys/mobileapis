// Internal Dependencies
const subItemHelper = require('../../../helpers/subItemHelper');

const subItemRoutes = {};

subItemRoutes.get = async (req, res, next) => {
  try {
    const params = req.body;
    const data = await subItemHelper.findAll(params, true);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

subItemRoutes.getOne = async (req, res, next) => {
  try {
    const id = req.params.id;
    const data = await subItemHelper.findAll({ id }, true, true);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

subItemRoutes.update = async (req, res, next) => {
  try {
    const id = req.params.id;
    const update = req.body;
    const data = await subItemHelper.findAndUpdate(id, update);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

subItemRoutes.create = async (req, res, next) => {
  try {
    const document = req.body;
    document.createdBy = req.user._id;
    document.ownedBy = req.user._id;
    const data = await subItemHelper.create(document);
    res.status(data.code).send(data);
  } catch (error) {
    console.log('\n error : ', error);
    next(error);
  }
};

subItemRoutes.delete = async (req, res, next) => {
  try {
    const id = req.params.id;
    const user_id = req.user._id
    const data = await subItemHelper.delete(id, user_id);
    res.status(data.code).send(data);
  } catch (error) {
    next(error);
  }
};

module.exports = subItemRoutes;
