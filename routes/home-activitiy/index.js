const express = require('express');
const router = express.Router();

/* Internal routers */
const homeActivity = require('./home-activity');

/* GET users listing. */
router.get('/', homeActivity.get);

module.exports = router;