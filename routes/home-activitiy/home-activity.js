const status = require('http-status');
const UserRoutes = {};

/* Helpers */
const homeActivityHelper = require('../../helpers/homeActivityHelper');

/* GET users listing. */
UserRoutes.get = async (req, res) => {
    try {
        const { appId } = req.body;
        if(appId && appId !== '') {
            const data = await homeActivityHelper.getHomeActivity(appId);
            res.status(data.code).json(data);
        } else {
            res.status(status.NOT_FOUND).json({
                code: status.NOT_FOUND,
                data: null,
                message: 'Please specify app Id'
            });
        }
    } catch(err) {
        res.status(status.INTERNAL_SERVER_ERROR).json({
            code: status.INTERNAL_SERVER_ERROR,
            message: 'something went wrong',
            error : err
        });
    }
};

module.exports = UserRoutes;
