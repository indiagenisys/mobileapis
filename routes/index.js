var express = require('express');
var router = express.Router();

const adminRoutes = require('./admin');
const authRoutes = require('./auth');
const homeActivityRoutes = require('./home-activitiy');

router.use('/admin', adminRoutes);
router.use('/auth', authRoutes);
router.use('/home-activitiy', homeActivityRoutes);

module.exports = router;