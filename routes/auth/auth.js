const status = require('http-status');

const AuthRoutes = {};

/* Helpers */
const AuthHelper = require('../../helpers/userHelper');
const { validationResult } = require('express-validator');

/* Register */
AuthRoutes.register = async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(status.BAD_REQUEST).json({
            code: status.BAD_REQUEST,
            message: 'BAD REQUEST',
            errors: errors.array().map(e => { return{ message: e.msg, field: e.param}})
        });
      };
      const data = await AuthHelper.create({ ...req.body, profileImage: req.file.path});
      res.status(data.code).json(data);
    } catch (error) {
      next(error);
    }
};

/* Login */
AuthRoutes.login = async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(status.BAD_REQUEST).json({
            code: status.BAD_REQUEST,
            message: 'BAD REQUEST',
            errors: errors.array().map(e => { return{ message: e.msg, field: e.param}})
        });
      }
      const data = await AuthHelper.login(req, res, next);
      res.status(data.code).json(data);
    } catch (error) {
      next(error);
    }
};

module.exports = AuthRoutes;
