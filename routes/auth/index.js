const express = require('express');
const router = express.Router();
const { checkSchema } = require('express-validator');
const { RegisterSchema } = require('./register.schema')
const upload = require('../../middleware/multiupload');

/* Internal routers */
const AuthRoutes = require('./auth');

/* GET users listing. */
router.post('/register', upload.single('profileImage'), checkSchema(RegisterSchema), AuthRoutes.register);
router.post('/login', AuthRoutes.login);

module.exports = router; 