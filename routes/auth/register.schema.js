const { Roles } = require('../../constants/enum');

const loginSchema = {
    password: {
        in: ['body'],
        errorMessage: "Password is required.",
        isLength: {
            options: {
                min: 8
            },
            errorMessage: "Password must be greater than 8.",
        }, 
    },  
    email: {
        in: ['body'],
        notEmpty: true,
        isEmail: {
            allow_display_name: true,
            errorMessage: 'email is in proper formate.'
        },
        errorMessage: 'email is required',
    },
};

const registrationSchema = {
    ...loginSchema,
    firstName: {
        notEmpty: true,
        errorMessage: 'firstName is required',
    },
    email: {
        notEmpty: true,
        isEmail: {
            allow_display_name: true,
            errorMessage: 'email is in proper formate.'
        },
        errorMessage: 'email is required',
    },
    lastName: {
        notEmpty: true,
        errorMessage: 'lastName is required',
    },
    userName: {
        notEmpty: true,
        errorMessage: 'userName is required',
    },
    role: {
        notEmpty: true,
        isIn: {
            options: [Object.values(Roles)],
            errorMessage: 'role is not proper'
        },
        errorMessage: 'role is required',
    }
};

const updateSchema = {
    id: {
        in: ['params'], 
        notEmpty: true,
        errorMessage: 'id is required',
    },
    firstName: {
        in: ['body'], 
        notEmpty: true,
        errorMessage: 'firstName is required',
    },
    lastName: {
        in: ['body'],
        notEmpty: true,
        errorMessage: 'lastName is required',
    },
    userName: {
        in: ['body'],
        notEmpty: true,
        errorMessage: 'userName is required',
    },
    role: {
        in: ['body'],
        notEmpty: true,
        isIn: {
            options: Object.values(Roles),
            errorMessage: 'role is not proper'
        },
        errorMessage: 'role is required',
    }
};

module.exports = {
    RegisterSchema: registrationSchema,
    LoginSchema: loginSchema,
    UpdateSchema: updateSchema
};