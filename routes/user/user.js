const status = require('http-status');
const { validationResult } = require('express-validator');
const userHelper = require('../../../helpers/userHelper');
// const upload = require('../../../middleware/multiupload');
const UserRoutes = {};

/* Helpers */

/* GET users listing. */
UserRoutes.get = async (req, res) => {
    const data = await userHelper.getAll(req.body);
    res.status(data.code).json(data);
};

/* GET one user record. */
UserRoutes.getOne = async (req, res) => {
    const { id } = req.params;
    const data = await userHelper.getOne(id);
    res.status(data.code).json(data);
};

/* Create user */
UserRoutes.create = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(status.BAD_REQUEST).json({
                code: status.BAD_REQUEST,
                message: 'BAD REQUEST',
                errors: errors.array().map(e => { return { message: e.msg, field: e.param}})
            });
        }
        const data = await userHelper.create({ ...req.body, profileImage: req.file.path});
        res.status(data.code).json(data);
    } catch(err) {
        res.status(status.INTERNAL_SERVER_ERROR).json({ code: 500, message: 'Internal server error', error: err});
    }
    
};

/* Update user */
UserRoutes.update = async (req, res) => {
    try {   
        const data = await userHelper.update(req.params.id, { ...req.body, profileImage: req.file.path });
        console.log('\n data : ', data);
        res.status(data.code).json(data);
    } catch(err) {
        res.status(status.INTERNAL_SERVER_ERROR).json({ code: 500, message: 'Internal server error', error: err});
    }
};

module.exports = UserRoutes;
