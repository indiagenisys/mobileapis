const express = require('express');
const router = express.Router();
const { checkSchema } = require('express-validator');
const validationSchema = require('../../auth/register.schema');
const upload = require('../../../middleware/multiupload');

/* Internal routers */
const user = require('./user');

/* GET users listing. */
router.get('/', user.get);

module.exports = router;