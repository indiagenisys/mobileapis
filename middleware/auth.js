const jwt = require("jsonwebtoken");
const { User } = require("../config/db");
const config = require('../config/config');

const auth = (roles = []) => {
  try {
    return [
      // authorize based on user role
      async (req, res, next) => {
        if(roles.length > 0) {
          const token = req.header("Authorization");
          if(token) {
            const replaceToken = token.replace("Bearer ", "");
            const decoded = jwt.verify(replaceToken, config.SECRET_KEY);
            if (!decoded) {
              return res.status(401).send({ Error: "Invalid Authentication!!!" });
            }
            const user = await User.findOne({ _id: decoded.id });
            if (user && roles.includes(user.role)) {
              req.user = user;
              req.token = token;
              return next();
            }     
            return res.status(401).send({ Error: "Invalid Authentication!!!" });
          }
          return res.status(401).send({ Error: "Invalid Authentication!!!" });
        } else {
          return next();
        }
      }
    ];
  } catch (e) {
    return res.status(401).send({ Error: "Please Authenticate!" });
  }
};

module.exports = auth;
