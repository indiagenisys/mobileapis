const encryption = require('../helpers/encryptionHelper');
const config = require('../config/config');

const security = async (req, res, next) => {
  try {
    if(config.IS_ENCRYPTED == 'true') {
      const { data } = req.body;
      let check = await encryption.checkEncryption(data);
      if (!check) {
        return res.status(401).send({ Error: "Unauthorized User!!!" });
      }
      const checkData = JSON.parse(check.data);
      req.body = { ...checkData, key: check.key, secret: check.secret };
    }
    next();
  } catch (e) {
    console.log('Error : ', e);
    res.status(401).send({ Error: "Please Authenticate!" });
  }
};

module.exports = security;
