const path = require("path");
const multer = require("multer");

// store file at locally
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    console.log('\n storageeeee ====> : ', file);
    return cb(null, 'uploads/');
  },
  filename: (req, file, cb) => {
    console.log('\n filenameeee : ', file);
    return cb(null, new Date().toISOString() + "-" + file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  const filetypes = /jpeg|jpg|png|/;
  const ffiletype = /ttf/;
  console.log('\n fileFilterrr ===> : ', req, file);
  const imgext = filetypes.test(path.extname(file.originalname).toLowerCase());
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    (file.mimetype === "image/jpeg" && imgext)
  ) {
    cb(null, true);
  } else {
    cb("Error: Images Only!");
  }
};

const upload = multer({ storage: storage, fileFilter: fileFilter });
// const upload = multer({ dest: 'public/images/' });

module.exports = upload;
