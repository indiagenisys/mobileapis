const path = require("path");
const multer = require("multer");

// store file at locally
const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    if (file.fieldname === "preview") {
      cb(null, "assets/preview");
    } else {
      cb("Error! Server errror!");
    }
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  const filetypes = /jpeg|jpg|png|/;
  const imgext = filetypes.test(path.extname(file.originalname).toLowerCase());

  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    (file.mimetype === "image/jpeg" && imgext)
  ) {
    cb(null, true);
  } else {
    cb("Error: Images Only!");
  }
};

const upload = multer({
  storage: fileStorage,
  fileFilter: fileFilter,
}).single("preview");

module.exports = upload;
