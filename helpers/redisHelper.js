// NPM Dependencies
const redis = require("redis");
require('redis-delete-wildcard')(redis);
const config = require('../config/config');
const TemplateHelper = require('./templateHelper');
var client = null;


// Helpers
const RedisHelper = {};

RedisHelper.conn = () => {
  try {
    client = redis.createClient(config.REDIS_URL);
    // //log error to the console if any occurs
    client.on("error", (err) => {
      console.log('\n Error in redis connection : ', err);
    });
  } catch (error) {
    console.log('\n Error in redis connection : ', error);
  }
};

RedisHelper.assignCollection = async (key, data, ex = false) => {

  try {
    return await client.set(key, JSON.stringify(data), 'EX', ex);
  } catch (error) {
    console.log('\n in Error Redis Assign Template : ', error);
    return false;
  }
};

RedisHelper.checkCollection = async (key) => {
  return await new Promise((resolve, reject) => {
    client.get(key, (err, res) => {
      if(res) resolve(JSON.parse(res));
      else resolve(false);
    });
  });
}

RedisHelper.removeCollectionWithPattern = (pattern) => {
  try {
    client.keys(pattern, function(err, rows) {
        if(rows && rows.length) {
          rows.forEach(r => {
              RedisHelper.deleteCollection(r);
          });
        }
    });
  } catch(err) {
    console.log('\n Err in remove collection : ', err);
  }
}

RedisHelper.deleteCollection = (key) => {
  client.del(key);
}

RedisHelper.flushAll = async() =>{
  client.flushall();
}

RedisHelper.getRedisString = async (redisObj) => {
  try {
    let redisArray = [];
    for (let i = 0; i < Object.keys(redisObj).length; i++) {
      if(redisObj[Object.keys(redisObj)[i]] !== '') 
        redisArray.push(redisObj[Object.keys(redisObj)[i]])
    }
    return redisArray.join('_');
  } catch(error) {
    console.log('\n Err in Redis string : ', error)
    return '';
  }
}

module.exports = RedisHelper;
