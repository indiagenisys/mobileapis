const status = require('http-status');

// Internal Dependencies
const { Tag } = require('../config/db');
const mongoose = require('mongoose');

const TagHelper ={};
const commonHelper = require('../helpers/commonHelper');

// Admin
TagHelper.findOne = async (id) => {
  const tag = await Tag.findById(id);
  return {
    data: tag,
    code: status.OK,
  };
};

// Public, Admin
TagHelper.findAll = async (params, isAdmin = false, isSingle= false) => {
  try {
    let matchData = {};
    let facetData = [];
    let data = [];
    if(!isAdmin) {
      matchData.isActive = true; 
    } else if(params && params.hasOwnProperty("isActive")) {
      matchData.isActive = params.isActive; 
    }
    if(params && params.hasOwnProperty('id') && params.id !== '') {
      matchData._id = mongoose.Types.ObjectId(params.id);
    }
  
    facetData = [
      {
        $lookup: {
          from: 'users',
          localField: 'createdBy',
          foreignField: '_id',
          as: 'user',
        },
      },
      {
          $unwind: '$user',
      },
      {
        $unwind: '$name'
      },
      {
        $lookup: {
          from: 'languages',
          localField: 'name.langId',
          foreignField: '_id',
          as: 'name.langId',
        },
      },
      {
        $lookup: {
          from: "items",
          localField: "_id",
          foreignField: "tags",
          as: 'items'
        }
      },
      {
        $group: {
          _id: '$_id',
          isActive: { $first: '$isActive' },
          name: { 
            $addToSet: { 
              value: '$name.value', 
              langCode: { $first: '$name.langId.code' },
              langName: { $first: '$name.langId.name' },
              langCountry: { $first: '$name.langId.country' },
              langIsActive: { $first: '$name.langId.isActive' },
              langCreatedAt: { $first: '$name.langId.createdAt' },
              langCreatedBy: { $first: '$name.langId.createdBy' }
            }
          },
          user: { 
            $first: { 
              firstName: '$user.firstName', 
              lastName: '$user.lastName', 
              roles: '$user.roles'
            }
          },
          items: { $first: '$items' },
          parent: { $first: '$parent'},
          createdAt: { $first: '$createdAt' },
          updatedAt: { $first: '$updatedAt' },
        },
      },
      {
        $project: {
          _id: 1,
          isActive: 1,
          name: 1,
          user: 1,
          itemCount: { $size: '$items' },
          parent: 1,
          createdAt: 1,
          updatedAt: 1,
        },
      },
      {
        $unwind: '$name'
      },
      {
        $lookup: {
          from: 'users',
          localField: 'name.langCreatedBy',
          foreignField: '_id',
          as: 'name.lang',
        },
      },
      {
        $group: {
          _id: '$_id',
          isActive: { $first: '$isActive' },
          name: { 
            $addToSet: {
              value: '$name.value', 
              lang: {
                code: '$name.langCode', 
                name: '$name.langName', 
                country: '$name.langCountry', 
                isActive: '$name.langIsActive', 
                createdAt: '$name.langCreatedAt', 
                createdBy: '$name.langCreatedBy', 
                createdBy: {
                  firstName: { $first: '$name.lang.firstName' },
                  lastName: { $first: '$name.lang.lastName' },
                  userName: { $first: '$name.lang.userName' },
                  role: { $first: '$name.lang.role' }
                }
              }
            }
          },
          user: { 
            $first: { 
              firstName: '$user.firstName', 
              lastName: '$user.lastName', 
              role: '$user.role'
            }
          },
          itemCount: { $first: '$itemCount' },
          parent: { $first: '$parent'},
          createdAt: { $first: '$createdAt' },
          updatedAt: { $first: '$updatedAt' },
        },
      },
      {
        $project: {
          _id: 1,
          defaultName: {
              $first: {
                $filter: {
                input: "$name",
                as: "n",
                cond: {
                  $eq: [ "$$n.lang.code", 'en-US']
                }
              }
            }
          },
          otherName: {
            $filter: {
              input: "$name",
              as: "n",
              cond: {
                $ne: [ "$$n.lang.code", 'en-US']
              }
            }
          },
          isActive: 1,
          user: 1,
          itemCount: 1,
          parent: 1,
          createdAt: 1,
          updatedAt: 1
        },
      }
    ];
    if (params.search && params.search !== '') {
      matchData.name = {
        $elemMatch: {
          "value": { $regex: params.search, $options: 'i' }
        }
      }
    }
    if(params.page && params.pageSize) {
      const page = parseInt(params.page) || 1;
      const limit = parseInt(params.pageSize) || 50;
      const skips = (page - 1) * limit;
      facetData.push(
        {
          $skip: skips,
        },
        {
          $limit: limit,
        }
      );
    }
    data = await Tag.aggregate([
      {
        $match: matchData,
      },
      {
        $facet: {
          data: facetData,
          count: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  
    const dataObj = {
      data: data.length ? (isSingle ? data[0].data[0] : data[0].data) : [],
      code: status.OK
    };
    if (data.length && data[0].count[0]) {
      dataObj.count = data[0].count[0].count;
    } else {
      dataObj.count = 0;
    }
    return dataObj;
  } catch(e) {
    throw e;
  }
}

TagHelper.findAndUpdate = async (id, update) => {
  try{
    const data = await Tag.findByIdAndUpdate(id, update, { new: true });
    return {
      data,
      message: 'Tag updated successfully!',
      code: status.OK,
    };
  } catch(e) {
    console.log('Error : ', e);
    return {
      data: null,
      message: 'Tag not updated successfully!',
      code: status.INTERNAL_SERVER_ERROR,
    };
  }
};

// Admin
TagHelper.create = async (document) => {
  try {
    return await commonHelper.create(Tag, document);
  } catch(e) {
    if(e.code == 11000) {
      return {
        data: null,
        message: 'Tag already created!',
        code: status.BAD_REQUEST,
      };
    } else {
      return {
        data: null,
        message: 'Tag not created successfully!',
        code: status.BAD_REQUEST,
      };
    }
  }
};

// Admin 
TagHelper.delete = async (id, user_id) => {
  await Tag.findByIdAndDelete(id);
  return {
    message: 'Tag deleted successfully!',
    code: status.OK,
  };
};

module.exports = TagHelper;