const status = require('http-status');

// Internal Dependencies
const { Item } = require('../config/db');
const mongoose = require('mongoose');

const ItemHelper = {};

// Admin
ItemHelper.findOne = async (id) => {
  const data = await Item.findById(id);
  return {
    data,
    code: status.OK,
  };
};

ItemHelper.getItemFacetData = async() => {
  return facetData = [
    {
      $lookup: {
        from: 'users',
        localField: 'createdBy',
        foreignField: '_id',
        as: 'createdBy',
      }
    },
    {
      $unwind: '$createdBy',
    },
    {
      $lookup: {
        from: 'users',
        localField: 'ownedBy',
        foreignField: '_id',
        as: 'ownedBy',
      }
    },
    {
      $unwind: '$ownedBy',
    },
    {
      $lookup: {
        from: 'apps',
        localField: 'apps',
        foreignField: '_id',
        as: 'apps',
      },
    },
    {
      $unwind: {
        path: '$apps',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $group: {
        _id: '$_id',
        isActive: { $first: '$isActive' },
        apps: { $addToSet: '$apps' },
        name: { $first: '$name' },
        createdBy: { $first: '$createdBy' },
        ownedBy: { $first: '$ownedBy' },
        categories: { $first: '$categories' },
        tags: { $first: '$tags' },
        createdAt: { $first: '$createdAt' },
        updatedAt: { $first: '$updatedAt' },
      },
    },
    {
      $project: {
        _id: 1,
        isActive: 1,
        name: 1,
        createdBy: 1,
        ownedBy: 1,
        categories: 1,
        tags: 1,
        apps:1,
        createdAt: 1,
        updatedAt: 1,
      },  
    },
    {
      $unwind: '$name'
    },
    {
      $lookup: {
        from: 'languages',
        localField: 'name.langId',
        foreignField: '_id',
        as: 'name.langId',
      },
    },
    {
      $group: {
        _id: '$_id',
        isActive: { $first: '$isActive' },
        name: { 
          $addToSet: { 
            value: '$name.value', 
            langCode: { $first: '$name.langId.code' },
            langName: { $first: '$name.langId.name' },
            langCountry: { $first: '$name.langId.country' },
            langIsActive: { $first: '$name.langId.isActive' },
            langCreatedAt: { $first: '$name.langId.createdAt' },
            langCreatedBy: { $first: '$name.langId.createdBy' }
          }
        },
        createdBy: { 
          $first: { 
            firstName: '$createdBy.firstName', 
            lastName: '$createdBy.lastName', 
            role: '$createdBy.role'
          }
        },
        ownedBy: { 
          $first: { 
            firstName: '$ownedBy.firstName', 
            lastName: '$ownedBy.lastName', 
            role: '$ownedBy.role'
          }
        },
        categories: { $first: '$categories' },
        tags: { $first: '$tags' }, 
        createdAt: { $first: '$createdAt' },
        apps: { $first: '$apps' },
        updatedAt: { $first: '$updatedAt' },
      }
    },
    {
      $project: {
        _id: 1,
        isActive: 1,
        name: 1,
        createdBy: 1,
        ownedBy: 1,
        categories: 1,
        tags: 1,
        createdAt: 1,
        updatedAt: 1,
        apps:1
      },  
    },
    {
      $unwind: '$name'
    },
    {
      $lookup: {
        from: 'users',
        localField: 'name.langCreatedBy',
        foreignField: '_id',
        as: 'name.lang',
      }
    },
    {
      $group: {
        _id: '$_id',
        isActive: { $first: '$isActive' },
        name: { 
          $addToSet: {
            value: '$name.value', 
            lang: {
              code: '$name.langCode', 
              name: '$name.langName', 
              country: '$name.langCountry', 
              isActive: '$name.langIsActive', 
              createdAt: '$name.langCreatedAt', 
              createdBy: '$name.langCreatedBy', 
              createdBy: {
                firstName: { $first: '$name.lang.firstName' },
                lastName: { $first: '$name.lang.lastName' },
                userName: { $first: '$name.lang.userName' },
                role: { $first: '$name.lang.role' }
              }
            }
          }
        },
        categories: { $first: '$categories' },
        tags: { $first: '$tags' },
        apps: { $first: '$apps' },
        createdBy: { $first: '$createdBy' },
        ownedBy: { $first: '$ownedBy' },
        createdAt: { $first: '$createdAt' },
        updatedAt: { $first: '$updatedAt' },
      }
    },
    {
      $project: {
        _id: 1,
        defaultName: {
            $first: {
              $filter: {
              input: "$name",
              as: "n",
              cond: {
                $eq: [ "$$n.lang.code", 'en-US']
              }
            }
          }
        },
        otherName: {
          $filter: {
            input: "$name",
            as: "n",
            cond: {
              $ne: [ "$$n.lang.code", 'en-US']
            }
          }
        },
        createdBy: 1,
        categories: 1,
        tags: 1,
        apps: 1,
        ownedBy: 1,
        isActive: 1,
        createdAt: 1,
        updatedAt: 1
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'categories',
        foreignField: '_id',
        as: 'categories',
      }
    },
    {
      $unwind: {
        path: '$categories',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unwind: {
        path: '$categories.name',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: 'languages',
        localField: 'categories.name.langId',
        foreignField: '_id',
        as: 'categories.name.langId',
      }
    },
    {
      $group: {
        _id: "$_id",
        categories: {
          $addToSet: {
            _id: '$categories._id',
            parentId: '$categories.parentId',
            createdBy: '$categories.createdBy',
            isActive: '$categories.isActive',
            name: {
              value: '$categories.name.value',
              lang: {
                code: { $first: '$categories.name.langId.code' },
                name: { $first: '$categories.name.langId.name' },
                country: { $first: '$categories.name.langId.country' },
                isActive: { $first: '$categories.name.langId.isActive' },
                createdAt: { $first: '$categories.name.langId.createdAt' },
                createdBy: { $first: '$categories.name.langId.createdBy' }
              }
            }
          }
        },
        defaultName: { $first: '$defaultName' },
        otherName: { $first: '$otherName' },
        createdBy: { $first: '$createdBy' },
        ownedBy: { $first: '$ownedBy' },
        tags: { $first: '$tags' },
        isActive: { $first: '$isActive' },
        createdAt: { $first: '$createdAt' },
        updatedAt: { $first: '$updatedAt' },
        apps: { $first: '$apps' }
      }
    },
    {
      $lookup: {
        from: 'tags',
        localField: 'tags',
        foreignField: '_id',
        as: 'tags',
      }
    },
    {
      $unwind: {
        path: '$tags',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unwind: {
        path: '$tags.name',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: 'languages',
        localField: 'tags.name.langId',
        foreignField: '_id',
        as: 'tags.name.langId',
      }
    },
    {
      $group: {
        _id: "$_id",
        tags: {
          $addToSet: {
            _id: '$tags._id',
            parentId: '$tags.parentId',
            createdBy: '$tags.createdBy',
            isActive: '$tags.isActive',
            name: {
              value: '$tags.name.value',
              lang: {
                code: { $first: '$tags.name.langId.code' },
                name: { $first: '$tags.name.langId.name' },
                country: { $first: '$tags.name.langId.country' },
                isActive: { $first: '$tags.name.langId.isActive' },
                createdAt: { $first: '$tags.name.langId.createdAt' },
                createdBy: { $first: '$tags.name.langId.createdBy' }
              }
            }
          }
        },
        defaultName: { $first: '$defaultName' },
        otherName: { $first: '$otherName' },
        createdBy: { $first: '$createdBy' },
        ownedBy: { $first: '$ownedBy' },
        categories: { $first: '$categories' },
        isActive: { $first: '$isActive' },
        createdAt: { $first: '$createdAt' },
        updatedAt: { $first: '$updatedAt' },
        apps: { $first: '$apps' }
      }
    },
    {
      $lookup: {
        from: 'subitems',
        localField: '_id',
        foreignField: 'itemId',
        as: 'subItems',
      }
    },
    {
      $project: {
        _id: 1,
        tags: 1,
        categories: 1,
        subItems: { $size: '$subItems'},
        defaultName: 1,
        otherName: 1,
        createdBy: 1,
        ownedBy: 1,
        isActive: 1,
        createdAt: 1,
        updatedAt: 1,
        apps: 1
      },
    }
  ];
}

// Public, Admin
ItemHelper.findAll = async (params, isAdmin = false, isSingle = false) => {
  let matchData = {};
  let facetData = await ItemHelper.getItemFacetData();
  let data = [];

  if (params) {
    if (params.hasOwnProperty('id') && params.id !== '') {
      matchData._id = mongoose.Types.ObjectId(params.id);
    }
    if (params.hasOwnProperty("isActive")) {
      matchData.isActive = params.isActive;
    }
    if(params.hasOwnProperty('search') && params.search !== '') {
      matchData.name = {
        $elemMatch: {
          "value": { $regex: params.search, $options: 'i' }
        }
      }
    }
    if (params.hasOwnProperty('page') && params.page != '' && params.hasOwnProperty('pageSize') && params.pageSize != '' ) {
      const page = parseInt(params.page) || 1;
      const limit = parseInt(params.pageSize) || 50;
      const skips = (page - 1) * limit;
      facetData.push(
        {
          $skip: skips,
        },
        {
          $limit: limit,
        }
      );
    }
    if (params.hasOwnProperty('categoryId') && params.categoryId !== '') {
      matchData.categories = {
        $elemMatch: { 
          $in: [mongoose.Types.ObjectId(params.categoryId)]
        }
      }
    }
    if (params.hasOwnProperty('tagId') && params.tagId !== '') {
      matchData.tags = {
        $elemMatch: { 
          $in: [mongoose.Types.ObjectId(params.tagId)]
        }
      }
    }
  }

  data = await Item.aggregate([
    {
      $match: matchData,
    },
    {
      $facet: {
        data: facetData,
        count: [
          {
            $count: 'count',
          },
        ],
      },
    },
  ]);


  const dataObj = {
    data: data.length ? data[0].data : [],
    code: status.OK
  };

  if (dataObj.data.length) {
    dataObj.count = data[0].count[0].count;
    dataObj.data = dataObj.data.map(d => {
      let categories = [];
      let tags = [];
      if(d.categories.length) {
        for (let i = 0; i < d.categories.length; i++) {
          const index = categories.findIndex(c => c._id.toString() == d.categories[i]._id.toString());
          if(index < 0) {
            categories.push({...d.categories[i], name: [d.categories[i].name]})
          } else {
            categories[index].name.push(d.categories[i].name); 
          } 
        }
      }
      if(d.tags.length) {
        for (let j = 0; j < d.tags.length; j++) {
          const index = tags.findIndex(c => c._id.toString() == d.tags[j]._id.toString());
          if(index == -1) {
            tags.push({...d.tags[j], name: [d.tags[j].name]})
          } else {
            tags[index].name.push(d.tags[j].name); 
          }
        }
      }
      return {
        ...d,
        categories,
        tags
      }
    });
    if(isSingle) 
      dataObj.data = dataObj.data[0]
  } else {
    dataObj.count = 0;
  }
  return dataObj;
}

ItemHelper.findAndUpdate = async (id, update) => {
  const exist = await Item.count({_id: mongoose.Types.ObjectId(id)});
  if(exist) {
    const data = await Item.findByIdAndUpdate(id, update, { new: true });
    return {
      data,
      message: 'Item updated successfully!',
      code: status.OK,
    };
  } else {
    return {
      data: null,
      message: 'Item not found!!',
      code: status.NOT_FOUND
    }
  }
};

// Admin
ItemHelper.create = async (document) => {
  try {
    const data = await Item.create(document);
    return {
      data,
      message: 'Item created successfully!',
      code: status.OK,
    };
  } catch (e) {
    console.log('\n Err : ', e);
    if (e.code == 11000) {
      return {
        data: null,
        message: 'Item already created!',
        code: status.BAD_REQUEST,
      };
    } else {
      return {
        data: null,
        message: 'Item not created successfully!',
        code: status.BAD_REQUEST,
      };
    }
  }
};

// Admin, 
ItemHelper.delete = async (id, user_id) => {
  const exist = await Item.count({_id: mongoose.Types.ObjectId(id)});
  if(exist) {
    await Item.findByIdAndDelete(id);
    return {
      message: 'Item deleted successfully!',
      code: status.OK,
    };
  } else {
    return {
      data: null,
      message: 'Item already deleted!!',
      code: status.NOT_FOUND
    }
  }

};

module.exports = ItemHelper;