const status = require('http-status');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../config/config');

// Internal Dependencies
const { User } = require('../config/db');

// Internal Helpers
const commonHelper = require('../helpers/commonHelper');

const UsersHelper ={};

UsersHelper.getJwtPayload = (user) => {
  return {
    firstName: user.firstName,
    lastName: user.lastName,
    id: user._id.toString(),
  };
};

// Admin
UsersHelper.getAll = async (params) => {
  try {
    let matchData = {
      'roles' : {
        $ne: 'Admin'
      }
    };
    let facetData = [];
  
    if (params.search && params.search.length) {
      matchData = {
        ...matchData,
        $or: [
          { "firstName": { $regex: params.search, $options: 'i' }}, 
          { "lastName": { $regex: params.search, $options: 'i' }},
          { "email": { $regex: params.search, $options: 'i' }},
          { "userName": { $regex: params.search, $options: 'i' }}
        ]
      }
    }
    if(params.page && params.pageSize) {
      const page = parseInt(params.page) || 1;
      const limit = parseInt(params.pageSize) || 50;
      const skips = (page - 1) * limit;
      facetData.push(
        {
          $skip: skips,
        },
        {
          $limit: limit,
        }
      ); 
    }
    return await commonHelper.findAllWithPaginationAndSearch(User, { facetData, matchData});
  } catch(err) {
    return {
      code: status.INTERNAL_SERVER_ERROR,
      message: err,
      data: null
    }
  }
}

// Admin
UsersHelper.getOne = async (id) => {
  try {
    const query = [
      {
        $match: {
          _id: mongoose.Types.ObjectId(id)
        }
      },
      {
        $project: {
          firstName: 1,
          lastName: 1,
          userName: 1,
          role: 1,
          isActive: 1,
          email: 1
        }
      }
    ]
    return await commonHelper.findByIdWithAggregate(User, query);
  } catch(err) {
    return {
      code: status.INTERNAL_SERVER_ERROR,
      message: 'Something went wrong.',
      data: null
    }
  }
};

// Admin 
UsersHelper.create = async (data) => {
  try {
    const { email, password, userName } = data;
    const findQuery = {
      $or: [
        { email : { $regex: '^' + email + '$' , $options: 'i' } },
        { userName: { $regex: '^' + userName + '$', $options: 'i' } }
      ]
    };
    const existingUser = await User.findOne(findQuery);
    if (existingUser) {
      if(existingUser.userName == data.userName) {
        return {
          message: 'Username already in use',
          code: status.CONFLICT,
          status: status[status.CONFLICT + '_NAME'],
        };
      }
      if(existingUser.email == data.email) {
        return {
          message: 'Email already in use',
          code: status.CONFLICT,
          status: status[status.CONFLICT + '_NAME'],
        };
      }
    } else {
      const hashedPassword = await bcrypt.hash(password, 8);
      const user = await commonHelper.create(User, { ...data, password: hashedPassword }); 
      return {
        token: jwt.sign(UsersHelper.getJwtPayload(user.data), config.SECRET_KEY, {
          expiresIn: 604800,
        }),
        user: user.data,
        code: status.CREATED,
      };
    }
  } catch(err) {
    console.log('\n Error :', err);
    return {
      code: status.INTERNAL_SERVER_ERROR,
      message: 'Something went wrong.',
      data: err
    }
  }
};

UsersHelper.login = async (req) => {
  try {
    const { email, password } = req.body;
    if (!email || !password) {
      return {
        message: 'Email and Password are requried',
        code: status.UNPROCESSABLE_ENTITY,
        status: status[status.UNPROCESSABLE_ENTITY+ '_NAME'],
      };
    }
    const user = await User.findOne({ email: { $regex: '^' + email + '$', $options: 'i' }});
    if (!user) {
      return {
        message: 'Invalid email or password',
        code: status.CONFLICT,
        status: status[status.CONFLICT+ '_NAME'],
      };
    }
    const match = await bcrypt.compare(password, user.password);
    if (!match) {
      return {
        message: 'Invalid email or password',
        code: status.CONFLICT,
        status: status[status.CONFLICT+ '_NAME'],
      };
    }
    return {
      token: jwt.sign(UsersHelper.getJwtPayload(user), config.SECRET_KEY, {
        expiresIn: 604800,
      }),
      user,
      code: status.OK,
    };
  } catch (error) {
    console.log('\n Error : ', error);
    return {
      error: error.message,
      code: status.INTERNAL_SERVER_ERROR,
      status: status[status.INTERNAL_SERVER_ERROR + '_NAME'],
    };
  }
};

// Admin 
UsersHelper.update = async (id, data) => {
  try {
    let updateData = { ...data };
    const { email, password, userName } = updateData;

    const findQuery = {
      $or: [
        { _id: { $ne: new mongoose.Types.ObjectId(id) } },
        { email : { $regex: '^' + email + '$' , $options: 'i' } },
        { userName: { $regex: '^' + userName + '$', $options: 'i' } }
      ]
    };
    const existingUser = await User.findOne(findQuery);
    if (existingUser) {
      if(existingUser.userName == userName) {
        return {
          message: 'Username already in use',
          code: status.CONFLICT,
          status: status[status.CONFLICT + '_NAME'],
        };
      }
      if(existingUser.email == email) {
        return {
          message: 'Email already in use',
          code: status.CONFLICT,
          status: status[status.CONFLICT + '_NAME'],
        };
      }
      // if(data && data.hasOwnProperty('password')) {
      //   const hashedPassword = await bcrypt.hash(password, 8);
      //   updateData.password = hashedPassword;
      // }
      const user = await commonHelper.updateById(User, id, updateData); 
      return {
        user: user,
        code: status.CREATED,
        message: 'User updated successfully'
      };
    } 
    return {
      user: null,
      code: status.INTERNAL_SERVER_ERROR,
      message: 'User not updated successfully'
    };
  } catch(err) {
    console.log('\n Error :', err);
    return {
      code: status.INTERNAL_SERVER_ERROR,
      message: 'Something went wrong.',
      data: err
    }
  }
};

module.exports = UsersHelper;