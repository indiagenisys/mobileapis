const status = require('http-status');
// Internal Dependencies
const { Category } = require('../config/db');
const mongoose = require('mongoose');

const CategoryHelper ={};
const commonHelper = require('../helpers/commonHelper');

// Admin
CategoryHelper.findOne = async (id) => {
  const category = await Category.findById(id);
  return {
    data: category,
    code: status.OK,
  };
};

CategoryHelper.findCategoryByParent = async (params) => {
  try {
    let matchData = {}; 
    let facetData = [
      {
        $project: {
          _id: 1,
          name: 1,
          createdAt: 1,
          parentId: 1
        }
      },
      {
        $sort: {
          createdAt : -1
        }
      }
    ];
    let data = [];
    if(params && params.hasOwnProperty('id') && params.id !== '') {
      if(params.id !== 'root')
        matchData.parentId = mongoose.Types.ObjectId(params.id);
      else
        matchData.parentId = null
    }
    if(params.page && params.pageSize) {
      const page = parseInt(params.page) || 1;
      const limit = parseInt(params.pageSize) || 50;
      const skips = (page - 1) * limit;
      facetData.push(
        {
          $skip: skips,
        },
        {
          $limit: limit,
        }
      );
    }

    data = await Category.aggregate([
      {
        $match: matchData,
      },
      {
        $facet: {
          data: facetData,
          count: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  
    const dataObj = {
      data: data.length ? data[0].data : [],
      code: status.OK
    };
    if (data.length && data[0].count[0]) {
      dataObj.count = data[0].count[0].count;
    } else {
      dataObj.count = 0;
    }
    return dataObj;
  } catch(error) {
    console.log('\n Error : ', error);
    return [];
  }
}
// Public, Admin
CategoryHelper.findAll = async (params, isAdmin = false, isSingle= false) => {
  let matchData = {};
  let facetData = [];
  let data = [];
  if(!isAdmin) {
    matchData.isActive = true; 
  } else if(params && params.hasOwnProperty("isActive")) {
    matchData.isActive = params.isActive; 
  }
  if(params && params.hasOwnProperty('id') && params.id !== '') {
    matchData._id = mongoose.Types.ObjectId(params.id);
  }
  if(params && params.hasOwnProperty('parentId') && params.parentId && params.parentId !== '') {
    matchData.parentId = mongoose.Types.ObjectId(params.parentId);
  }
    
  if (params && params.hasOwnProperty('search') && params.search !== '') {
    matchData.name = {
      $elemMatch: {
        "value": { $regex: params.search, $options: 'i' }
      }
    }
  }

  facetData = [
    {
      $lookup: {
        from: 'users',
        localField: 'createdBy',
        foreignField: '_id',
        as: 'user',
      },
    },
    {
        $unwind: '$user',
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'parentId',
        foreignField: '_id',
        as: 'parent',
      },
    },
    {
      $unwind: {
        path: '$parent',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unwind: '$name'
    },
    {
      $lookup: {
        from: 'languages',
        localField: 'name.langId',
        foreignField: '_id',
        as: 'name.langId',
      },
    },
    {
      $lookup: {
        from: "items",
        localField: "_id",
        foreignField: "categories",
        as: 'items'
      }
    },
    {
      $group: {
        _id: '$_id',
        isActive: { $first: '$isActive' },
        name: { 
          $addToSet: { 
            value: '$name.value', 
            langCode: { $first: '$name.langId.code' },
            langName: { $first: '$name.langId.name' },
            langCountry: { $first: '$name.langId.country' },
            langIsActive: { $first: '$name.langId.isActive' },
            langCreatedAt: { $first: '$name.langId.createdAt' },
            langCreatedBy: { $first: '$name.langId.createdBy' }
          }
        },
        user: { 
          $first: { 
            firstName: '$user.firstName', 
            lastName: '$user.lastName', 
            roles: '$user.roles'
          }
        },
        items: { $first: '$items' },
        parent: { $first: '$parent'},
        createdAt: { $first: '$createdAt' },
        updatedAt: { $first: '$updatedAt' },
      },
    },
    {
      $project: {
        _id: 1,
        isActive: 1,
        name: 1,
        user: 1,
        itemCount: { $size: '$items' },
        parent: 1,
        createdAt: 1,
        updatedAt: 1,
      },  
    },
    {
      $unwind: '$name'
    },
    {
      $lookup: {
        from: 'users',
        localField: 'name.langCreatedBy',
        foreignField: '_id',
        as: 'name.lang',
      },
    },
    {
      $group: {
        _id: '$_id',
        isActive: { $first: '$isActive' },
        name: { 
          $addToSet: {
            value: '$name.value', 
            lang: {
              code: '$name.langCode', 
              name: '$name.langName', 
              country: '$name.langCountry', 
              isActive: '$name.langIsActive', 
              createdAt: '$name.langCreatedAt', 
              createdBy: '$name.langCreatedBy', 
              createdBy: {
                firstName: { $first: '$name.lang.firstName' },
                lastName: { $first: '$name.lang.lastName' },
                userName: { $first: '$name.lang.userName' },
                role: { $first: '$name.lang.role' }
              }
            }
          }
        },
        user: { 
          $first: { 
            firstName: '$user.firstName', 
            lastName: '$user.lastName', 
            role: '$user.role'
          }
        },
        itemCount: { $first: '$itemCount' },
        parent: { $first: '$parent'},
        createdAt: { $first: '$createdAt' },
        updatedAt: { $first: '$updatedAt' },
      },
    },
    {
      $project: {
        _id: 1,
        defaultName: {
            $first: {
              $filter: {
              input: "$name",
              as: "n",
              cond: {
                $eq: [ "$$n.lang.code", 'en-US']
              }
            }
          }
        },
        otherName: {
          $filter: {
            input: "$name",
            as: "n",
            cond: {
              $ne: [ "$$n.lang.code", 'en-US']
            }
          }
        },
        isActive: 1,
        user: 1,
        itemCount: 1,
        parent: 1,
        createdAt: 1,
        updatedAt: 1
      },
    }
  ];

  if(params && params.hasOwnProperty('page') && params.page !=='' && params.hasOwnProperty('pageSize') && params.pageSize !=='') {
    const page = parseInt(params.page) || 1;
    const limit = parseInt(params.pageSize) || 50;
    const skips = (page - 1) * limit;
    facetData.push(
      {
        $skip: skips,
      },
      {
        $limit: limit,
      }
    );
  }
  data = await Category.aggregate([
    {
      $match: matchData,
    },
    {
      $facet: {
        data: facetData,
        count: [
          {
            $count: 'count',
          },
        ],
      },
    },
  ]);

  const dataObj = {
    data: data.length ? (isSingle ? data[0].data[0] : data[0].data) : [],
    code: status.OK
  };
  if (data.length && data[0].count[0]) {
    dataObj.count = data[0].count[0].count;
  } else {
    dataObj.count = 0;
  }
  return dataObj;
}

CategoryHelper.findAndUpdate = async (id, update) => {
  try{
    const data = await Category.findByIdAndUpdate(id, update, { new: true });
    return {
      data,
      message: 'Category updated successfully!',
      code: status.OK,
    };
  } catch(e) {
    console.log('Error : ', e);
    return {
      data: null,
      message: 'Category not updated successfully!',
      code: status.INTERNAL_SERVER_ERROR,
    };
  }
};

// Admin
CategoryHelper.create = async (document) => {
  try {
    return await commonHelper.create(Category, document);
  } catch(e) {
    if(e.code == 11000) {
      return {
        data: null,
        message: 'Category already created!',
        code: status.BAD_REQUEST,
      };
    } else {
      return {
        data: null,
        message: 'Category not created successfully!',
        code: status.BAD_REQUEST,
        error: e
      };
    }
  }
};

// Admin 
CategoryHelper.delete = async (id, user_id) => {
  await Category.count({_id: mongoose.Types.ObjectId(id)});
  if(exist) {
    await Category.findByIdAndDelete(id);
    return {
      message: 'Category deleted successfully!',
      code: status.OK,
    };
  } else {
    return {
      data: null,
      message: 'Category already deleted!!',
      code: status.NOT_FOUND
    }
  }
};

module.exports = CategoryHelper;