const status = require('http-status');
// Internal Dependencies
const { Language, User } = require('../config/db');

const LanguageHelper ={};

// Default Language Create
LanguageHelper.defaultCreate = async (req, res, next) => {
  try {
    let defaultObj = {
      code: 'en-US',
    }
    const admin = await User.findOne({ role: 'Admin'});
    if(admin) {
      defaultObj.createdBy = admin._id;
      const checkExist = await Language.find(defaultObj).count();
      if(checkExist == 0) {
        defaultObj.name = 'English';
        defaultObj.country = 'United States';
        await Language.create(defaultObj);
      }    
    }
    return next();
  } catch(e) {
    console.log('Error : ', e);
    return next();
  }
};

// Admin
LanguageHelper.findOne = async (id) => {
  const language = await Language.findById(id);
  return {
    data: language,
    code: status.OK,
  };
};

// Public, Admin
LanguageHelper.findAll = async (params, isAdmin = false) => {
  let matchData = { };
  let facetData = [];
  let data = [];
  if(!isAdmin) {
    matchData.isActive = true; 
    facetData = [
      {
        $project: {
          _id: 1,
          name: 1,
          country: 1,
          code: 1, 
          isActive: 1
        },
      },
    ];
  } else {
    if(params && params.hasOwnProperty("isActive")) {
      matchData.isActive = params.isActive; 
    }
    facetData = [
      {
        $lookup: {
        from: 'users',
        localField: 'createdBy',
        foreignField: '_id',
        as: 'user',
        },
      },
      {
          $unwind: '$user',
      },
      {
        $project: {
          _id: 1,
          name: 1,
          code: 1,
          country: 1,
          isActive: 1,
          user:{ firstName: "$user.firstName", lastName: "$user.lastName", _id: "$user._id" },
        },
      },
    ];
  }
  if(params.all && params.all !== undefined && params.all === true){
      data = await Language.aggregate([
        {
          $match: matchData
        },
        {
            $facet: {
                data: facetData,
                count: [
                    {
                    $count: 'count',
                    },
                ],
            },
        },
      ]);
   
  }else{
    const page = parseInt(params.page) || 1;
    const limit = parseInt(params.pageSize) || 50;
    const skips = (page - 1) * limit;
    if (params.searchValue && params.searchValue.length) {
      matchData.name = { $regex: params.searchValue, $options: 'i' };
    }
    facetData.push(
      {
        $skip: skips,
      },
      {
        $limit: limit,
      }
    );

    data = await Language.aggregate([
      {
        $match: matchData,
      },
      {
        $facet: {
          data: facetData,
          count: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  const dataObj = {
    data: data.length ? data[0].data : [],
    code: status.OK
  };
  if (data.length && data[0].count[0]) {
    dataObj.count = data[0].count[0].count;
  } else {
    dataObj.count = 0;
  }
  return dataObj;
}

LanguageHelper.findAndUpdate = async (id, update) => {
  const language = await Language.findByIdAndUpdate(id, update, { new: true });
  return {
    data: language,
    message: 'Language updated successfully!',
    code: status.OK,
  };
};

// Admin
LanguageHelper.create = async (document) => {
  try {
    const language = await Language.create(document);
    return {
        data: language,
        message: 'Language created successfully!',
        code: status.OK,
    };
  } catch(e) {
    if(e.code == 11000) {
      return {
        data: null,
        message: 'Language already created!',
        code: status.BAD_REQUEST,
      };
    } else {
      return {
        data: null,
        message: 'Language not created successfully!',
        code: status.BAD_REQUEST,
      };
    }
  }
};

// Admin, 
LanguageHelper.delete = async (id, user_id) => {
  await Language.findByIdAndDelete(id);
  return {
    message: 'Language deleted successfully!',
    code: status.OK,
  };
};

module.exports = LanguageHelper;