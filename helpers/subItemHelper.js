const status = require('http-status');

// Internal Dependencies
const { SubItem } = require('../config/db');
const mongoose = require('mongoose');

const SubItemHelper = {};

// Admin
SubItemHelper.findOne = async (id) => {
  const data = await SubItem.findById(id);
  return {
    data,
    code: status.OK,
  };
};

// Public, Admin
SubItemHelper.findAll = async (params, isAdmin = false, isSingle = false) => {
  let matchData = {};
  let facetData = [];
  let data = [];

  facetData = [
    {
      $lookup: {
        from: 'items',
        localField: 'itemId',
        foreignField: '_id',
        as: 'item',
      }
    },
    {
      $unwind: {
        path: '$item',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unwind: '$name'
    },
    {
      $lookup: {
        from: 'languages',
        localField: 'name.langId',
        foreignField: '_id',
        as: 'name.langId',
      },
    },
    {
      $group: {
        _id: '$_id',
        isActive: { $first: '$isActive' },
        name: { 
          $addToSet: { 
            value: '$name.value', 
            langCode: { $first: '$name.langId.code' },
            langName: { $first: '$name.langId.name' },
            langCountry: { $first: '$name.langId.country' },
            langIsActive: { $first: '$name.langId.isActive' },
            langCreatedAt: { $first: '$name.langId.createdAt' },
            langCreatedBy: { $first: '$name.langId.createdBy' }
          }
        }, 
        item: { $first: '$item' },
        createdAt: { $first: '$createdAt' },
        updatedAt: { $first: '$updatedAt' },
      }
    },
    {
      $project: {
        _id: 1,
        isActive: 1,
        name: 1,
        item: 1,
        createdAt: 1,
        updatedAt: 1,
      },  
    },
    {
      $unwind: '$name'
    },
    {
      $lookup: {
        from: 'users',
        localField: 'name.langCreatedBy',
        foreignField: '_id',
        as: 'name.lang',
      }
    },
    {
      $group: {
        _id: '$_id',
        isActive: { $first: '$isActive' },
        name: { 
          $addToSet: {
            value: '$name.value', 
            lang: {
              code: '$name.langCode', 
              name: '$name.langName', 
              country: '$name.langCountry', 
              isActive: '$name.langIsActive', 
              createdAt: '$name.langCreatedAt', 
              createdBy: '$name.langCreatedBy', 
              createdBy: {
                firstName: { $first: '$name.lang.firstName' },
                lastName: { $first: '$name.lang.lastName' },
                userName: { $first: '$name.lang.userName' },
                role: { $first: '$name.lang.role' }
              }
            }
          }
        },
        item: { $first: '$item' },
        createdAt: { $first: '$createdAt' },
        updatedAt: { $first: '$updatedAt' },
      }
    },
    {
      $project: {
        _id: 1,
        defaultName: {
            $first: {
              $filter: {
              input: "$name",
              as: "n",
              cond: {
                $eq: [ "$$n.lang.code", 'en-US']
              }
            }
          }
        },
        otherName: {
          $filter: {
            input: "$name",
            as: "n",
            cond: {
              $ne: [ "$$n.lang.code", 'en-US']
            }
          }
        },
        item: 1,
        isActive: 1,
        createdAt: 1,
        updatedAt: 1
      },
    },
    {
      $unwind: '$item.name',
    },
    {
      $lookup: {
        from: 'languages',
        localField: 'item.name.langId',
        foreignField: '_id',
        as: 'item.name.langId',
      }
    },
    {
      $group: {
        _id: "$_id",
        item: {
          $addToSet: {
            _id: '$item._id',
            parentId: '$item.parentId',
            createdBy: '$item.createdBy',
            isActive: '$item.isActive',
            name: {
              value: '$item.name.value',
              lang: {
                code: { $first: '$item.name.langId.code' },
                name: { $first: '$item.name.langId.name' },
                country: { $first: '$item.name.langId.country' },
                isActive: { $first: '$item.name.langId.isActive' },
                createdAt: { $first: '$item.name.langId.createdAt' },
                createdBy: { $first: '$item.name.langId.createdBy' }
              }
            }
          }
        },
        defaultName: { $first: '$defaultName' },
        otherName: { $first: '$otherName' },
        isActive: { $first: '$isActive' },
        createdAt: { $first: '$createdAt' },
        updatedAt: { $first: '$updatedAt' }
      }
    },
  ];

  if (params) {
    if (params.hasOwnProperty('itemId') && params.itemId !== '') {
      matchData.itemId = mongoose.Types.ObjectId(params.itemId);
    }
    if (params.hasOwnProperty('id') && params.id !== '') {
      matchData._id = mongoose.Types.ObjectId(params.id);
    }
    if (params && params.hasOwnProperty("isActive")) {
      matchData.isActive = params.isActive;
    }  
    if(params.hasOwnProperty('search') && params.search !== '') {
      matchData.name = {
        $elemMatch: {
          "value": { $regex: params.search, $options: 'i' }
        }
      }
    }
    if (params.hasOwnProperty('page') && params.page != '' && params.hasOwnProperty('pageSize') && params.pageSize != '' ) {
      const page = parseInt(params.page) || 1;
      const limit = parseInt(params.pageSize) || 50;
      const skips = (page - 1) * limit;
      facetData.push(
        {
          $skip: skips,
        },
        {
          $limit: limit,
        }
      );
    }
  }

  data = await SubItem.aggregate([
    {
      $match: matchData,
    },
    {
      $facet: {
        data: facetData,
        count: [
          {
            $count: 'count',
          },
        ],
      },
    },
  ]);

  const dataObj = {
    data: data.length ? (isSingle ? data[0].data[0] : data[0].data) : [],
    code: status.OK
  };
  if (data.length && data[0].count[0]) {
    dataObj.count = data[0].count[0].count;
  } else {
    dataObj.count = 0;
  }
  return dataObj;
}

SubItemHelper.findAndUpdate = async (id, update) => {
  const exist = await SubItem.count({_id: mongoose.Types.ObjectId(id)});
  if(exist) {
    const data = await SubItem.findByIdAndUpdate(id, update, { new: true });
    return {
      data,
      message: 'SubItem updated successfully!',
      code: status.OK,
    };
  } else {
    return {
      data: null,
      message: 'SubItem not found!!',
      code: status.NOT_FOUND
    }
  }
  
};

// Admin
SubItemHelper.create = async (document) => {
  try {
    const data = await SubItem.create(document);
    return {
      data,
      message: 'SubItem created successfully!',
      code: status.OK,
    };
  } catch (e) {
    console.log('\n Err : ', e);
    if (e.code == 11000) {
      return {
        data: null,
        message: 'SubItem already created!',
        code: status.BAD_REQUEST,
      };
    } else {
      return {
        data: null,
        message: 'SubItem not created successfully!',
        code: status.BAD_REQUEST,
      };
    }
  }
};

// Admin, 
SubItemHelper.delete = async (id, user_id) => {
  const exist = await SubItem.count({_id: mongoose.Types.ObjectId(id)});
  if(exist) {
    await SubItem.findByIdAndDelete(id);
    return {
      message: 'SubItem deleted successfully!',
      code: status.OK,
    };
  } else {
    return {
      data: null,
      message: 'SubItem already deleted!!',
      code: status.NOT_FOUND
    }
  }
};

module.exports = SubItemHelper;