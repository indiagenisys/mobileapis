const status = require('http-status');

// Internal Dependencies
const { App } = require('../config/db');
const mongoose = require('mongoose');

const AppHelper ={};
const commonHelper = require('../helpers/commonHelper');

// Admin
AppHelper.findOne = async (id) => {
  const data = await App.findById(id);
  return {
    data,
    code: status.OK,
  };
};

// Public, Admin
AppHelper.findAll = async (params, isAdmin = false, isSingle= false) => {
  let matchData = {};
  let facetData = [];
  let data = [];

  if (params) {
    if (params.hasOwnProperty('id') && params.id !== '') {
      matchData._id = mongoose.Types.ObjectId(params.id);
    }
    if (params.hasOwnProperty("isActive")) {
      matchData.isActive = params.isActive;
    }
    if(params.hasOwnProperty('search') && params.search !== '') {
      matchData.name = { $regex: params.search, $options: 'i' }
    }
    if (params.hasOwnProperty('page') && params.page != '' && params.hasOwnProperty('pageSize') && params.pageSize != '' ) {
      const page = parseInt(params.page) || 1;
      const limit = parseInt(params.pageSize) || 50;
      const skips = (page - 1) * limit;
      facetData.push(
        {
          $skip: skips,
        },
        {
          $limit: limit,
        }
      );
    }
    if (params.hasOwnProperty('storeId') && params.storeId !== '') {
      matchData.stores = {
        $elemMatch: { 
          $in: [mongoose.Types.ObjectId(params.storeId)]
        }
      }
    }
  }


  facetData = [
    {
      $lookup: {
        from: 'users',
        localField: 'createdBy',
        foreignField: '_id',
        as: 'user',
      },
    },
    {
        $unwind: '$user',
    },
    {
      $lookup: {
        from: 'stores',
        localField: 'stores',
        foreignField: '_id',
        as: 'stores',
      },
    },
    {
      $unwind: {
        path: '$stores',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $group: {
        _id: '$_id',
        isActive: { $first: '$isActive' },
        name: { $first: '$name' },
        packageName: { $first: '$packageName' },
        stores: { 
          $addToSet: '$stores'
        },
        user: { 
          $first: { 
            firstName: '$user.firstName', 
            lastName: '$user.lastName', 
            roles: '$user.roles'
          }
        },
        createdAt: { $first: '$createdAt' },
        updatedAt: { $first: '$updatedAt' },
      },
    },
    {
      $project: {
        _id: 1,
        isActive: 1,
        name: 1,
        packageName: 1,
        stores: 1,
        storesCount: { $size: '$stores' },
        user: 1,
        createdAt: 1,
        updatedAt: 1,
      },  
    }
  ];

  if(params && params.hasOwnProperty('page') && params.page !=='' && params.hasOwnProperty('pageSize') && params.pageSize !=='') {
    const page = parseInt(params.page) || 1;
    const limit = parseInt(params.pageSize) || 50;
    const skips = (page - 1) * limit;
    facetData.push(
      {
        $skip: skips,
      },
      {
        $limit: limit,
      }
    );
  }

  data = await App.aggregate([
    {
      $match: matchData,
    },
    {
      $facet: {
        data: facetData,
        count: [
          {
            $count: 'count',
          },
        ],
      },
    },
  ]);

  const dataObj = {
    data: data.length ? (isSingle ? data[0].data[0] : data[0].data) : [],
    code: status.OK
  };

  if (data.length && data[0].count[0]) {
    dataObj.count = data[0].count[0].count;
  } else {
    dataObj.count = 0;
  }
  
  return dataObj;
};

// Admin, Public
AppHelper.findAndUpdate = async (id, update) => {
  try{
    const data = await App.findByIdAndUpdate(id, update, { new: true });
    return {
      data,
      message: 'App updated successfully!',
      code: status.OK,
    };
  } catch(e) {
    console.log('Error : ', e);
    if(e.code == 11000) {
      return {
        data: null,
        message: 'App already created!',
        code: status.BAD_REQUEST,
      };
    } else {
      return {
        data: null,
        message: 'App not updated successfully!',
        code: status.BAD_REQUEST,

      };
    }
  }
};

// Admin
AppHelper.create = async (document) => {
  try {
    return await commonHelper.create(App, document);
  } catch(e) {
    if(e.code == 11000) {
      return {
        data: null,
        message: 'App already created!',
        code: status.BAD_REQUEST,
      };
    } else {
      return {
        data: null,
        message: 'App not created successfully!',
        code: status.BAD_REQUEST,

      };
    }
  }
};

// Admin 
AppHelper.delete = async (id, user_id) => {
  await App.count({_id: mongoose.Types.ObjectId(id)});
  if(exist) {
    await App.findByIdAndDelete(id);
    return {
      message: 'App deleted successfully!',
      code: status.OK,
    };
  } else {
    return {
      data: null,
      message: 'App already deleted!!',
      code: status.NOT_FOUND
    }
  }
};

module.exports = AppHelper;