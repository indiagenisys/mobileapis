const status = require('http-status');

// Internal Dependencies
const { Store } = require('../config/db');
const mongoose = require('mongoose');

const StoreHelper ={};
const commonHelper = require('../helpers/commonHelper');

// Admin
StoreHelper.findOne = async (id) => {
  const data = await Store.findById(id);
  return {
    data,
    code: status.OK,
  };
};

// Public, Admin
StoreHelper.findAll = async (params, isAdmin = false, isSingle= false) => {
  let matchData = {};
  let facetData = [];
  let data = [];
  if (params) {
    if (params.hasOwnProperty('id') && params.id !== '') {
      matchData._id = mongoose.Types.ObjectId(params.id);
    }
    if (params.hasOwnProperty("isActive")) {
      matchData.isActive = params.isActive;
    }
    if(params.hasOwnProperty('search') && params.search !== '') {
      matchData.name = { $regex: params.search, $options: 'i' }
    }
    if (params.hasOwnProperty('page') && params.page != '' && params.hasOwnProperty('pageSize') && params.pageSize != '' ) {
      const page = parseInt(params.page) || 1;
      const limit = parseInt(params.pageSize) || 50;
      const skips = (page - 1) * limit;
      facetData.push(
        {
          $skip: skips,
        },
        {
          $limit: limit,
        }
      );
    }
  }

  facetData = [
    {
      $lookup: {
        from: 'users',
        localField: 'createdBy',
        foreignField: '_id',
        as: 'user',
      },
    },
    {
        $unwind: '$user',
    },
    {
      $lookup: {
        from: 'apps',
        localField: '_id',
        foreignField: 'stores',
        as: 'apps',
      },
    },
    {
      $unwind: {
        path: '$apps',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $group: {
        _id: '$_id',
        isActive: { $first: '$isActive' },
        name: { $first: '$name' },
        packageName: { $first: '$packageName' },
        apps: { 
          $addToSet: '$apps'
        },
        user: { 
          $first: { 
            firstName: '$user.firstName', 
            lastName: '$user.lastName', 
            roles: '$user.roles'
          }
        },
        createdAt: { $first: '$createdAt' },
        updatedAt: { $first: '$updatedAt' },
      },
    },
    {
      $project: {
        _id: 1,
        isActive: 1,
        name: 1,
        packageName: 1,
        apps: 1,
        appsCount: { $size: '$apps' },
        user: 1,
        createdAt: 1,
        updatedAt: 1,
      },  
    }
  ];

  data = await Store.aggregate([
    {
      $match: matchData,
    },
    {
      $facet: {
        data: facetData,
        count: [
          {
            $count: 'count',
          },
        ],
      },
    },
  ]);

  const dataObj = {
    data: data.length ? (isSingle ? data[0].data[0] : data[0].data) : [],
    code: status.OK
  };
  if (data.length && data[0].count[0]) {
    dataObj.count = data[0].count[0].count;
  } else {
    dataObj.count = 0;
  }
  return dataObj;
};

// Admin, Public
StoreHelper.findAndUpdate = async (id, update) => {
  try{
    const data = await Store.findByIdAndUpdate(id, update, { new: true });
    return {
      data,
      message: 'Store updated successfully!',
      code: status.OK,
    };
  } catch(e) {
    console.log('Error : ', e);
    if(e.code == 11000) {
      return {
        data: null,
        message: 'Store already created!',
        code: status.BAD_REQUEST,
      };
    } else {
      return {
        data: null,
        message: 'Store not updated successfully!',
        code: status.BAD_REQUEST,

      };
    }
  }
};

// Admin
StoreHelper.create = async (document) => {
  try {
    return await commonHelper.create(Store, document);
  } catch(e) {
    if(e.code == 11000) {
      return {
        data: null,
        message: 'Store already created!',
        code: status.BAD_REQUEST,
      };
    } else {
      return {
        data: null,
        message: 'Store not created successfully!',
        code: status.BAD_REQUEST,

      };
    }
  }
};

// Admin 
StoreHelper.delete = async (id, user_id) => {
  const exist = await Store.count({_id: mongoose.Types.ObjectId(id)});
  if(exist) {
    await Store.findByIdAndDelete(id);
    return {
      message: 'Store deleted successfully!',
      code: status.OK,
    };
  } else {
    return {
      data: null,
      message: 'Store already deleted!!',
      code: status.NOT_FOUND
    }
  }
};

module.exports = StoreHelper;