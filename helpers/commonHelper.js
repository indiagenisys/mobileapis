// Internal Dependencies
const status = require('http-status');
const logger = require('../middleware/logger')

const CommonHelper = {};

// Get all record with pagination and search
CommonHelper.findAllWithPaginationAndSearch = async function (Model, params) {
  try {
    const getRecords = await Model.aggregate([
      {
        $match: params.matchData,
      },
      {
        $facet: {
          data: params.facetData,
          count: [
            {
              $count: 'count',
            },
          ],
        },
      }
    ]);
    const data = {
      records: getRecords.length ? getRecords[0].data : [],
    };
    if (getRecords.length && getRecords[0].count[0]) {
      data.count = getRecords[0].count[0].count;
    } else {
      data.count = 0;
    }
    return {
      data,
      code: status.OK,
      message: 'data get successfully.',
    };
  } catch (err) {
    logger.error(status.INTERNAL_SERVER_ERROR, err);
    throw err;
  }
}

// Get single record with id
CommonHelper.findById = async function(Model, id) {
  try {
    const data = await Model.findById(id);
    return {
      data,
      message: 'Record get successfully.',
      code: status.OK
    }
  } catch (err) {
    logger.error(status.INTERNAL_SERVER_ERROR, err);
    throw err;
  }
}

// Get single record with feilds and aggregation
CommonHelper.findByIdWithAggregate = async function(Model, params) {
  try {
    const data = await Model.aggregate(params);;
    if(data && data.length) {
      return {
        data : data[0],
        message: 'Record get successfully.',
        code: status.OK
      }
    } else {
      return {
        data : null,
        message: 'Record is not available.',
        code: status.OK
      }
    }
  } catch(err) {
    logger.error(status.INTERNAL_SERVER_ERROR, err);
    throw err ;
  }
}

// Create user
CommonHelper.create = async function(Model, data) {
  try {
    const user = await Model.create(data);
    return {
      data: user,
      message: 'Record created successfully.',
      code: status.OK
    }
  } catch (err) {
    logger.error(status.INTERNAL_SERVER_ERROR, err);
    throw err;
  }
}

// Create user
CommonHelper.updateById = async function(Model, id, data) {
  try {
    const updateData = await Model.findByIdAndUpdate(id, data, { new: true });
    return updateData;
  } catch (err) {
    logger.error(status.INTERNAL_SERVER_ERROR, err);
    throw err;
  }
}

// Admin, 
CommonHelper.deleteById = async function (Model, id) {
  try {
    const data = await Model.findByIdAndDelete(id); 
    return {
      message: 'Data deleted successfully!',
      code: status.OK,
    };
  } catch(err) {
    logger.error(status.INTERNAL_SERVER_ERROR, err);
    throw err;
  }
};

module.exports = CommonHelper;