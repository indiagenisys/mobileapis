// NPM Dependencies
const status = require('http-status');
const crypto = require('crypto');
const config = require('../config/config');

// Internal Dependencies
const { Setting } = require('../config/db');

// Helpers
const EncryptionHelper = {};

EncryptionHelper.checkEncryption = async (encryptedData) => {
  try {
    const keySecrets = await Setting.find({}).sort({createdAt: -1}).lean().exec();
    bodyDecrypt = false;
    var count = 0;
    var keyCheck = keySecrets[count];
    if(keyCheck && keyCheck.key && keyCheck.secret) {
      while(!bodyDecrypt){
        bodyDecrypt = await EncryptionHelper.decryption(encryptedData, keyCheck.key, keyCheck.secret);
        keyCheck = keySecrets[count];
        count++;
      };
      if(bodyDecrypt) {
        bodyDecrypt = JSON.parse(bodyDecrypt);
        await Setting.findByIdAndUpdate(keyCheck._id, { lastUpdated: Date.now()}, { new: true});
        let tokenDecrypt = await EncryptionHelper.decryption(bodyDecrypt.token, keyCheck.key, keyCheck.secret);
        let tokenSplit = tokenDecrypt.split(':|:');
        if(tokenSplit.length > 2 && tokenSplit[2] !== '') {
          let eT = tokenSplit[2];
          let cT = new Date().getTime();
          if(cT <= eT) {
            return { data: bodyDecrypt.data, key: keyCheck.key, secret: keyCheck.secret};
          }
        } 
      }
    }
    return false;
  } catch(e) {
    console.log('Error : ', e);
    return false;
  }
};

EncryptionHelper.encryption = async (message, key, secret) => {
  try {
    let iv = EncryptionHelper.generateIV(secret);
    let md5 = crypto.createHash('md5').update(key).digest('hex');
    const cipher = crypto.createCipheriv(
      config.ENCRYPTION_TYPE,
      new Buffer.from(md5, 'hex'),
      new Buffer.from(iv)
    );
    // cipher.setAutoPadding(true);
    var encrypted = cipher.update(message, 'utf8', 'base64');
    encrypted += cipher.final('base64');
    return encrypted;
  } catch(e) {
    return false;
  }
};

EncryptionHelper.responseService = async (req, res, responseObj) => {
  const encData = await EncryptionHelper.encryption(JSON.stringify(responseObj), req.body.key, req.body.secret);
  return res.status(status.OK).send({ data: encData});
};

EncryptionHelper.response = async (req, res, data) => {
  if(config.IS_ENCRYPTED == 'true')
    return responseService(req, res, data)
  else 
    return res.json(data);
};

EncryptionHelper.generateIV = (secret) => {
  var bytes = []; // char codes
  for (var i = 0; i < secret.length; ++i) {
    var code = secret.charCodeAt(i);
    bytes = bytes.concat([code]);
  }
  return bytes;
};

EncryptionHelper.decryption = async (text, key, secret) => {
  try {
  const iv = await EncryptionHelper.generateIV(secret);
  let md5 = crypto.createHash('md5').update(key).digest('hex');
  const decipher = crypto.createDecipheriv(
      'aes-128-cbc',
      new Buffer.from(md5, 'hex'),
      new Buffer.from(iv)
  );
  var decrypted = decipher.update(text, 'base64', 'utf8');
  decrypted += decipher.final('utf8');
  return decrypted;
  } catch(e) {
    return false;
  }
};

module.exports = EncryptionHelper;
