const status = require('http-status');

// Internal Dependencies
const { HomeActivity, Item} = require('../config/db');
const mongoose = require('mongoose');
const moment = require('moment');

const HomeActivityHelper ={};
const commonHelper = require('../helpers/commonHelper');
const ItemHelper = require('../helpers/itemHelper');

// Admin
HomeActivityHelper.findOne = async (id) => {
  const data = await HomeActivity.findById(id);
  return {
    data,
    code: status.OK,
  };
};

// Admin
HomeActivityHelper.findAll = async (params, isAdmin = false, isSingle= false) => {
  let matchData = {};
  let facetData = [];
  let data = [];

  if (params) {
    if (params.hasOwnProperty('id') && params.id !== '') {
      matchData._id = mongoose.Types.ObjectId(params.id);
    }
    if (params.hasOwnProperty("isActive")) {
      matchData.isActive = params.isActive;
    }
    if(params.hasOwnProperty('search') && params.search !== '') {
      matchData.name = { $regex: params.search, $options: 'i' }
    }
    if (params.hasOwnProperty('page') && params.page != '' && params.hasOwnProperty('pageSize') && params.pageSize != '' ) {
      const page = parseInt(params.page) || 1;
      const limit = parseInt(params.pageSize) || 50;
      const skips = (page - 1) * limit;
      facetData.push(
        {
          $skip: skips,
        },
        {
          $limit: limit,
        }
      );
    }
  }

  facetData = [
    {
      $lookup: {
        from: 'users',
        localField: 'createdBy',
        foreignField: '_id',
        as: 'user',
      }
    },
    {
      $unwind: '$user',
    },
    {
      $group: {
        _id: '$_id',
        isActive: { $first: '$isActive' },
        title: { $first: '$title' }, 
        type: { $first: '$type' }, 
        gridCount: { $first: '$gridCount' }, 
        itemCount: { $first: '$itemCount' }, 
        conditions: { $first: '$conditions' }, 
        scrollType: { $first: '$scrollType' }, 
        sequence: { $first: '$sequence' }, 
        isMoreBuotton: { $first: '$isMoreBuotton' },
        user: { 
          $first: { 
            firstName: '$user.firstName', 
            lastName: '$user.lastName', 
            roles: '$user.roles'
          }
        },
        createdBy: { $first: '$createdBy' },
        deletedBy: { $first: '$deletedBy'},
        createdAt: { $first: '$createdAt' },
        updatedAt: { $first: '$updatedAt' },
      },
    },
    {
      $project: {
        _id: 1,
        isActive: 1,
        title: 1,
        type: 1,
        gridCount: 1,
        itemCount: 1,
        conditions: 1,
        scrollType: 1,
        sequence: 1,
        isMoreBuotton: 1,
        user: 1,
        createdBy: 1,
        deletedBy: 1,
        createdAt: 1,
        updatedAt: 1,
      } 
    },
  ];

  data = await HomeActivity.aggregate([
    {
      $match: matchData,
    },
    {
      $facet: {
        data: facetData,
        count: [
          {
            $count: 'count',
          },
        ],
      },
    }
  ]);

  const dataObj = {
    data: data.length ? (isSingle ? data[0].data[0] : data[0].data) : [],
    code: status.OK
  };

  if (data.length && data[0].count[0]) {
    dataObj.count = data[0].count[0].count;
  } else {
    dataObj.count = 0;
  }
  
  return dataObj;
};

HomeActivityHelper.getHomeActivity = async (appId) => {
  try {
    const activities = await HomeActivity.aggregate([
      {
        $match: {
          isActive: false
        },
      },
      {
        $lookup: {
          from: 'apps',
          localField: 'appId',
          foreignField: '_id',
          as: 'apps',
        },
      },
      {
        $unwind: '$apps'
      },
      {
        $match: {
          'apps.packageName': appId
        }
      }
    ]);
    let response = [];
    for (let i = 0; i < activities.length; i++) {
      const data = await getDataFromCondition(activities[i]);
      const activity = {
        title: activities[i].title,
        gridCount: activities[i].gridCount,
        itemCount: activities[i].itemCount,
        scrollType: activities[i].scrollType,
        sequence: activities[i].sequence,
        isMoreBuotton: activities[i].isMoreBuotton,
        data
      }
      response.push(activity)
    }
    return {
      data: response,
      code: status.OK,
      message: 'Get Record successfully.'
    }
  } catch (err) {
    console.log('\n Err : ', err);
    throw err;
  }
}

const getDataFromCondition = async (data) => {
  try {
    let matchData = {};
    let sorting = {};

    const conditions = data.conditions;
    for (let i = 0; i < conditions.length; i++) {
      const con = conditions[i];
      switch (con.type) {
        case 'arithmetic':
          if(con.field == 'name') {
            matchData[con.field] = {
              $elemMatch: {
                "value": { $regex: con.value, $options: 'i' }
              }
            }
          } else if(['ne', 'eq', 'gt', 'gte', 'lt', 'lte', 'regex'].includes(con.operator)) {
            let op = {};
            op[`$${con.operator}`] = con.value;
            matchData[con.field] = op;
          }
          break;

        case 'sorting':
          if(con.value == 'asc')
            sorting[con.field] = 1;
          if(con.value == 'desc')
            sorting[con.field] = -1;
          break;

        case 'timeSpan':
            const checkTime = moment.utc().subtract(con.value, 'hours');
            matchData.createdAt = { $gte: new Date(checkTime) }
          break;

        case 'category':
          matchData.categories = {
            $elemMatch: { 
              $in: [mongoose.Types.ObjectId(con.value)]
            }
          }
          break;

        default:
          break;
      }
    }
    const facetData = await ItemHelper.getItemFacetData();
    const query = [];
    if(matchData && Object.keys(matchData).length) {
      query.push({ $match: matchData });
    }
    query.push(...facetData);
    if(sorting && Object.keys(sorting).length) {
      query.push({ $sort: sorting });
    }

    const res = await Item.aggregate(query);
    return res.map(d => {
      let categories = [];
      let tags = [];
      if(d.categories.length) {
        for (let i = 0; i < d.categories.length; i++) {
          const index = categories.findIndex(c => c._id.toString() == d.categories[i]._id.toString());
          if(index < 0) {
            categories.push({...d.categories[i], name: [d.categories[i].name]})
          } else {
            categories[index].name.push(d.categories[i].name); 
          } 
        }
      }
      if(d.tags.length) {
        for (let j = 0; j < d.tags.length; j++) {
          const index = tags.findIndex(c => c._id.toString() == d.tags[j]._id.toString());
          if(index == -1) {
            tags.push({...d.tags[j], name: [d.tags[j].name]})
          } else {
            tags[index].name.push(d.tags[j].name); 
          }
        }
      }
      return {
        ...d,
        categories,
        tags
      }
    });
  } catch (err) {
    throw err;
  }
}

// Admin
HomeActivityHelper.findAndUpdate = async (id, update) => {
  try{
    const data = await HomeActivity.findByIdAndUpdate(id, update, { new: true });
    return {
      data,
      message: 'HomeActivity updated successfully!',
      code: status.OK,
    };
  } catch(e) {
    console.log('Error : ', e);
    if(e.code == 11000) {
      return {
        data: null,
        message: 'HomeActivity already created!',
        code: status.BAD_REQUEST,
      };
    } else {
      return {
        data: null,
        message: 'HomeActivity not updated successfully!',
        code: status.BAD_REQUEST,

      };
    }
  }
};

// Admin
HomeActivityHelper.create = async (document) => {
  try {
    return await commonHelper.create(HomeActivity, document);
  } catch(e) {
    if(e.code == 11000) {
      return {
        data: null,
        message: 'HomeActivity already created!',
        code: status.BAD_REQUEST,
      };
    } else {
      return {
        data: null,
        message: 'HomeActivity not created successfully!',
        code: status.BAD_REQUEST,
        error: e
      };
    }
  }
};

// Admin 
HomeActivityHelper.delete = async (id, user_id) => {
  await HomeActivity.count({_id: mongoose.Types.ObjectId(id)});
  if(exist) {
    await HomeActivity.findByIdAndDelete(id);
    return {
      message: 'HomeActivity deleted successfully!',
      code: status.OK,
    };
  } else {
    return {
      data: null,
      message: 'HomeActivity already deleted!!',
      code: status.NOT_FOUND
    }
  }
};

module.exports = HomeActivityHelper;