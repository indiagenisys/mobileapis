var express = require('express');
var path = require('path');
const dotenv = require("dotenv");
const loggerDev = require('morgan');
dotenv.config();
const config = require('./config/config');

var app = express();

const bodyParser = require('body-parser');
const cors = require('cors');

const LanguageHelper = require('./helpers/languageHelper');

// Parsers for POST data
app.use(loggerDev('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "public")));
app.use(LanguageHelper.defaultCreate);

const allRoutes = require('./routes');

app.use(cors());
app.use('/', allRoutes);

app.get("/", (req, res) => {
  res.send(`${config.PROJECT_NAME} API Working`);
});

app.all('/*', (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header(
    'Access-Control-Allow-Headers',
    'Content-type,Accept,X-Access-Token,X-Key'
  );
  res.header('Access-Control-Expose-Headers', ' total-records');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'dev' ? err : {};
  console.log('\n Error : ', err);
  // render the error page
  res.status(err.status || 500);
  res.send('Error');
});

module.exports = app;
