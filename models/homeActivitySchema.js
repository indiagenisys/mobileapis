const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

const ActivityConditionSchema = new mongoose.Schema(
  {
    field: {
      type: String,
      required: true
    },
    type: {  /* [arithmetic, sorting, timeSpan, category] */
      type: String,
      required: true
    },
    operator: {
      type: String,
      required: false
    },
    appId: {
      type: ObjectId,
      required: true,
      ref: 'App'
    },
    value: {
      type: String,
      required: false
    },
    hoursCount: {
      type: Number,
      required: false
    },
    isActive: {
      type: Boolean,
      required: true,
      default: false
    }
  }
);

const HomeActivitySchema = new mongoose.Schema(
  {
    title: {
      type: String,
      unique: true,
      required: true
    },
    type: { /* [banner, category, activity] */
      type: String,
      required: true
    },
    gridCount: {
      type: Number,
      required: false
    },
    itemCount: {
      type: Number,
      required: true,
      default: 20
    },
    conditions: [ActivityConditionSchema],
    scrollType: {
      type: String,
      required: true,
      default: 'verticle'
    },
    sequence: {
      type: Number,
      required: true
    },
    isMoreBuotton: {
      type: Boolean,
      required: true,
      default: false
    },
    createdBy: {
      type: ObjectId,
      required: true,
      ref: "User",
    },
    deletedBy: {
      type: ObjectId,
      required: false,
      ref: "User",
    },
  },
  { 
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt'
    }
  },
  { strict:false }
);

module.exports = HomeActivitySchema;
