const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

const LanguageSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      index: true
    },
    createdBy: {
      type: ObjectId,
      required: true,
      ref: "User",
    },
    deletedBy: {
      type: ObjectId,
      required: false,
      ref: "User",
    },
    code: {
      type: String,
      unique: true,
      required: true,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: false
    },
    country: {
      type: String,
      required: true,
    }
  },
  { timestamps: true }
);

module.exports = LanguageSchema;
