const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

const CategorySchema = new mongoose.Schema(
  {
    name: [
      {
        value: {
          type: String,
          unique: true,
          required: true
        },
        langId: {
          type: ObjectId,
          required: true,
          ref: 'languages'
        }
      }
    ],
    parentId: {
      type: ObjectId,
      ref: 'User',
      default: null
    },
    image: {
      type: String,
      required: false
    },
    background: {
      type: String,
      required: false
    },
    createdBy: {
      type: ObjectId,
      required: true,
      ref: "User",
    },
    deletedBy: {
      type: ObjectId,
      required: false,
      ref: "User",
    },
    isActive: {
      type: Boolean,
      required: true,
      default: false
    },
  },
  { timestamps: true }
);

CategorySchema.index(
  {
    '_id': 1,
    'name.langId': 1
  },
  { unique: true }
)

module.exports = CategorySchema;
