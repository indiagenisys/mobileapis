const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

const AppSchema = new mongoose.Schema(
  {
    name:  {
      type: String,
      unique: true,
      required: true
    },
    stores: [
      {
        type: ObjectId,
        ref: 'Store'
      }
    ],
    packageName: {
      type: String,
      required: true
    },
    createdBy: {
      type: ObjectId,
      required: true,
      ref: "User",
    },
    deletedBy: {
      type: ObjectId,
      required: false,
      ref: "User",
    },
    isActive: {
      type: Boolean,
      required: true,
      default: false
    }
  },
  { 
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt'
    }
  },
  { strict:false }
);

module.exports = AppSchema;
