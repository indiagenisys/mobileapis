const UserSchema = require('./userSchema');
const { Roles } = require('../constants/enum')
UserSchema.pre('save', function (next) {
    
  const email = this.get('email');
  if (email) {
    this.email = this.email.toLowerCase();
  }

  const firstName = this.firstName;
  if (firstName) {
    this.set('firstName', firstName.trim());
  }

  const lastName = this.get('lastName');
  if (lastName) {
    this.set('lastName', lastName.trim());
  }
  if (this.role.length == 0) {
    this.role.push(Roles.ContentManager);
  }

  next();
});

UserSchema.virtual('fullName').get(function () {
  return `${this.firstName} ${this.lastName}`;
});

UserSchema.virtual('isAdmin').get(function () {
  return this.role.includes('Admin');
});

module.exports =  UserSchema;