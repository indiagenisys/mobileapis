const mongoose = require("mongoose");
const ObjectId = mongoose.Schema.Types.ObjectId;
const { Roles } = require('../constants/enum');

const UserSchema = new mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    userName: {
      type: String,
      unique: true,
      required: true,
    },
    email: {
      type: String,
      unique: true,
      required: true,
    },
    userType: {
      type: String,
      enum: ['dummy', 'real'],
      default: 'real'
    },
    password: {
      type: String,
      required: true,
    },
    profileImage: {
      type: String,
      required: true,
    },
    role: {
      type: String,
      enum: Object.values(Roles),
      default: 'Content Manager',
    },
    isActive: {
      type: Boolean,
      default: true,
      required: false,
    },
    lastLoginAt: {
      type: Date,
      required: false,
    },
  },
  { 
    timestamps: true 
  }
);

module.exports = UserSchema;
