const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

const StoreSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      unique: true,
      required: true
    },
    baseUrl: {
      type: String,
      required: true
    },
    createdBy: {
      type: ObjectId,
      required: true,
      ref: "User",
    },
    deletedBy: {
      type: ObjectId,
      required: false,
      ref: "User",
    },
    isActive: {
      type: Boolean,
      required: true,
      default: false
    }
  },
  { 
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt'
    }
  },
  { strict:false }
);

module.exports = StoreSchema;
