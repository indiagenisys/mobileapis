const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

const itemSchema = new mongoose.Schema(
  {
    name: [
      {
        value: {
          type: String,
          unique: true,
          required: true
        },
        langId: {
          type: ObjectId,
          required: true,
          ref: 'languages'
        }
      }
    ],
    createdBy: {
      type: ObjectId,
      required: true,
      ref: "User",
    },
    ownedBy: {
      type: ObjectId,
      required: true,
      ref: "User",
    },
    deletedBy: {
      type: ObjectId,
      required: false,
      ref: "User",
    },
    apps: [
      {
        type: ObjectId,
        ref: 'App'
      }
    ],
    categories: [
      {
        type: ObjectId,
        ref: 'Category'
      }
    ],
    tags: [
      {
        type: ObjectId,
        ref: 'Tag'
      }
    ],
    isActive: {
      type: Boolean,
      required: true,
      default: false
    },
  },
  { 
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt'
    }
  },
  { strict:false }
);

module.exports = itemSchema;
