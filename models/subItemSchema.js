const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

const subItemSchema = new mongoose.Schema(
  {
    name: [
      {
        value: {
          type: String,
          unique: true,
          required: true
        },
        langId: {
          type: ObjectId,
          required: true,
          ref: 'languages'
        }
      }
    ],
    itemId: {
      type: ObjectId,
      required: true,
      ref: "Item",
    },
    isActive: {
      type: Boolean,
      required: true,
      default: false
    },
  },
  { 
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt'
    }
  },
  { strict:false }
);

module.exports = subItemSchema;
