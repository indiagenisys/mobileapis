const Roles = {
    Admin : 'Admin',
    ContentManager: 'Content Manager',
    QA: 'QA',
    USER: 'User'
}

module.exports = {
    Roles: Roles
}