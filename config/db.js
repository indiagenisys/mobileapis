const mongoose = require("mongoose");
mongoose.connect(process.env.CONNECTION_STRING, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  // useFindAndModify :false
});
mongoose.Promise = global.Promise;

const UserSchema = require('../models/user-hooks-and-virtuals');
const LanguageSchema = require('../models/languageSchema');
const CategorySchema = require('../models/categorySchema');
const TagSchema = require('../models/tagSchema');
const ItemSchema = require('../models/itemSchema');
const SubItemSchema = require('../models/subItemSchema');
const AppSchema = require('../models/appSchema');
const StoreSchema = require('../models/storeSchema');
const HomeActivitySchema = require('../models/homeActivitySchema');

module.exports = {
  User: mongoose.model('User', UserSchema),
  Language: mongoose.model('Language', LanguageSchema),
  Category: mongoose.model('Category', CategorySchema),
  Tag: mongoose.model('Tag', TagSchema),
  Item: mongoose.model('Item', ItemSchema),
  SubItem: mongoose.model('SubItem', SubItemSchema),
  Store: mongoose.model('Store', StoreSchema),
  App: mongoose.model('App', AppSchema),
  HomeActivity: mongoose.model('HomeActivity', HomeActivitySchema)
};
